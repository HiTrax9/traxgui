#pragma once
#include "MainWindow.h"

class CApp
{
public:
	CApp(HINSTANCE hInstance);
	~CApp();
	bool IsDlgMessage(MSG * msg);
private:
	std::shared_ptr<CMainWindow> WndMain;
	std::shared_ptr<CXDialog> pOtherWindow;
};
