#include "App.h"

CApp::CApp(HINSTANCE hInstance)
{
	WndMain = std::make_shared<CMainWindow>(hInstance, L"WindowTitle");
	if (WndMain->Create())
		exit(1);
}

CApp::~CApp()
{
}

bool CApp::IsDlgMessage(MSG * msg)
{
	bool bIsDlgMsg = false;				// for this to work we need to use another vector or map that can handle "dialog messages"
	for (auto p : CXMessageHandlerWindow::Handlers()) // i say we make the CXMainWindow and CXDialog classes inherit from another class that implements said capability.
	{
		bIsDlgMsg = bIsDlgMsg || IsDialogMessage(p->GetHandle(), msg);
		if (bIsDlgMsg)
			break;
	}
	//bIsDlgMsg = bIsDlgMsg || IsDialogMessage(WndMain->GetHandle(), msg);
	return bIsDlgMsg;
}