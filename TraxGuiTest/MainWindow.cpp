#include "MainWindow.h"
#include "resource.h"

int MainWndBtnCallback(void* pData)
{
	auto pMainWnd = CXMainWindow::Get();
	switch (uintptr_t(pData))
	{
		case 0:
		{
			CXMainWindow::Get()->_dpc<CMainWindow>()->ShowDialog();
			break;
		}
		case ID_MENU_FILE_OPEN:
		{
			MsgBox(L"OpenFile", L"ClickedMenuItem");
		}
	}
	return 0;
}

CMainWindow::CMainWindow(HINSTANCE hInstance, string szTitle)
	: CXMainWindow(hInstance, szTitle)
{
	SetHeight(GetHeight() + 100);
	SetBgColor(RGB(25, 25, 25));
	SetTextColor(RGB(200, 200, 200));
}

CMainWindow::~CMainWindow()
{
}

int CMainWindow::CreateControls()
{
	dlgWindow = std::make_shared<DialogOne>(GetPointer());

	auto cm = GetControlManager();

	auto pBtn = cm->CreateButton(Button_MakeCP(ID_BTNSHOWDLG, 5, 5, 100, 25, L"Show Dialog"));
	pBtn->SetEventHandler(CLICK_EID, MainWndBtnCallback, 0);
	pBtn->Anchor(EANCHORDIR::INPLACE);

	auto pGB = cm->CreateGroupbox(_MakeCP_S(200, 0, 300, 400, L"Groupies!!"));
	pGB->Anchor(EANCHORDIR::INPLACE);
	auto pBMP = pGB->AddBitmap(_Bitmap_MakeCP(0, 5, 5, 150, 50));
	pBMP->Load(L"banner.bmp");

	auto pIcon = pGB->AddIcon(Icon_MakeCP(0, 160, 5));
	pIcon->Load(L"icon.ico");

	pGB->AddLabel(_MakeCP_S_(200, 5, L"Fuck a joooobbb!!"));
	pGB->AddButton(_MakeCP_S_(5, 55, L"Buttons"));
	pGB->AddCheckbox(_MakeCP_S_(5, 75, L"FECKFECKFECK!"));
	pGB->AddRadio(_MakeCP_S_(5, 95, L"Radio button!"));
	pGB->AddCombobox(_MakeCP_S(120, 55, 100, 25, L"Combobox!"));
	pGB->AddEdit(_MakeCP(120, 80));
	auto pLB = pGB->AddListBox(ListBox_MakeCP(0, 5, 120, 200, 100));
	pLB->AddItem(L"First Item!");
	pLB->AddItem(L"Second Item!!!", (void*)0xdeadbeef);
	auto pLV = pGB->AddListView(ListView_MakeCP(0, 5, 215, 200, 75));
	pLV->InsertColumn(L"Column One");
	pLV->InsertColumn(L"Column Two");
	CXListViewItem item(0, 0, L"Item 1");
	item.AddSubItem(0, 1, L"Sub-Item 1");
	pLV->AddItem(item);
	auto pGGB = pGB->AddGroupbox(Groupbox_MakeCP(0, 5, 295, 280, 80, L"Groupception"));
	pGGB->AddLabel(Label_MakeCP_(0, 10, 10, L"LOLS! =D"));
	CreateMenus();
	ReverseTabOrder();
	return 0;
}

void CMainWindow::ShowDialog()
{
	dlgWindow->Show();
}

void CMainWindow::CreateMenus()
{
	auto pMenu = CXMenu::Make();
	auto pFile = pMenu->AddItem(XMenuItem::Make(ID_MENU_FILE, L"File", true));
	auto pSubMenu = pFile->GetSubMenu();
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_FILE_OPEN, L"&Open\tAlt+O", MainWndBtnCallback));
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_FILE_SAVE, L"Save"));
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_FILE_EXIT, L"Exit"));
	auto pEdit = pMenu->AddItem(XMenuItem::Make(ID_MENU_EDIT, L"Edit", true));
	pSubMenu = pEdit->GetSubMenu();
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_EDIT_CUT, L"Cut"));
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_EDIT_COPY, L"Copy"));
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_EDIT_PASTE, L"Paste"));
	auto pView = pMenu->AddItem(XMenuItem::Make(ID_MENU_VIEW, L"View", true));
	pSubMenu = pView->GetSubMenu();
	pSubMenu->AddItem(XMenuItem::Make(ID_MENU_VIEW_WE, L"Whatever"));
	SetMenu(pMenu);

	pSubMenu = pMenu->GetItems()[ID_MENU_FILE]->GetSubMenu();
	auto pContext = CXMenu::Make(true);
	pContext->AddItem(pSubMenu->GetItems()[ID_MENU_FILE_OPEN]);
	pContext->AddItem(pSubMenu->GetItems()[ID_MENU_FILE_SAVE]);
	pContext->AddItem(pSubMenu->GetItems()[ID_MENU_FILE_EXIT]);
	SetContextMenu(pContext);

	//auto xMenu = CXMenu::Make(true);
	//auto pItem = XMenuItem::Make(666, L"Menu");
	//pItem->SetOwnerMenu(xMenu);
	//auto pSubMenu = pItem->AddSubMenu();//
	//pSubMenu->AddItem(XMenuItem::Make(667, L"Subbie", MainWndBtnCallback));
	//auto pSubItem = XMenuItem::Make(668, L"SubbieSub");
	//auto pSubMenu2 = pSubItem->AddSubMenu();
	//pSubMenu->AddItem(pSubItem);
	//pSubMenu2->AddItem(XMenuItem::Make(669, L"SubbitySubSub"));
	//xMenu->AddItem(pItem);
	//
	////SetMenu(xMenu);
	//SetContextMenu(xMenu);
}