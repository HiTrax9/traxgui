#pragma once
#include "stdafx.h"

#define ID_EDIT1		0x8000
#define ID_BTN_WRITE	0x8001
#define ID_BTN_READ		0x8002
#define ID_BTN_REMOVE	0x8003
#define ID_BTN_CLEAR	0x8004
#define ID_LB_LIST		0x8005

class DialogOne : public CXDialog
{
public:
	DialogOne(PXWindow& pOwner);
	static PXDialog pDialogOne;
	~DialogOne();
	int CreateControls();
	static void Write();
	static void Read();
	static void Remove();
	static void Clear();
};
