#include "DialogOne.h"
#include "resource.h"
PXDialog DialogOne::pDialogOne = nullptr;

int DlgOneCallback(void* pData)
{
	auto pMainWnd = CXMainWindow::Get();
	switch (uintptr_t(pData))
	{
		case 0:
		{
			DialogOne::Write();
			break;
		}
		case 1:
		{
			DialogOne::Read();
			break;
		}
		case 2:
		{
			DialogOne::Remove();
			break;
		}
		case 3:
		{
			DialogOne::Clear();
			break;
		}
	}
	return 0;
}

DialogOne::DialogOne(PXWindow & pOwner)
	: CXDialog(pOwner->GetInstance(), pOwner->GetWndProc(), L"Dialog Title")
{
}

DialogOne::~DialogOne()
{
}

int DialogOne::CreateControls()
{
	pDialogOne = GetPointer()->_dpc<CXDialog>();
	auto cm = GetControlManager(); //do i really need to explain this?

	auto pGB = cm->CreateGroupbox(_MakeCP_S(5, 0, 265, 170, L"Groupbox"));
	//auto pLbl = pGB->AddControl<CXLabel>(Label_MakeCP(0, 5, 15, 50, 0, L"Label:"));

	auto pEdit = pGB->AddControl<CXEdit>(Edit_MakeCP(ID_EDIT1, 5, 10, 0, 0, L"Edit control..."));

	pEdit->SetBuddyLabel(L"Buddy Label", EB_BOTTOMRIGHT, pGB);

	auto pBtn = pGB->AddControl<CXButton>(Button_MakeCP(ID_BTN_WRITE, 5, 55, 100, 20, L"Write"));
	pBtn->SetEventHandler(CLICK_EID, DlgOneCallback, 0);

	//pBtn = pGB->AddControl<CXButton>(Button_MakeCP(ID_BTN_READ, 5, 80, 100, 20, L"Read"));
	pBtn = pGB->AddButton(Button_MakeCP(ID_BTN_READ, 5, 80, 100, 20, L"Read"));
	pBtn->SetEventHandler(CLICK_EID, DlgOneCallback, 1);

	pBtn = pGB->AddControl<CXButton>(Button_MakeCP(ID_BTN_REMOVE, 5, 105, 100, 20, L"Remove"));
	pBtn->SetEventHandler(CLICK_EID, DlgOneCallback, 2);

	pBtn = pGB->AddControl<CXButton>(Button_MakeCP(ID_BTN_CLEAR, 5, 130, 100, 20, L"Clear"));
	pBtn->SetEventHandler(CLICK_EID, DlgOneCallback, 3);

	pGB->AddControl<CXListBox>(ListBox_MakeCP_(ID_LB_LIST, 110, 12, 150, 155, L"Simple Listbox"));

	return 0;
}

void DialogOne::Write()
{
	auto cm = pDialogOne->GetControlManager();
	string str = cm->GetControl<CXEdit>(ID_EDIT1)->GetText();
	cm->GetControl<CXListBox>(ID_LB_LIST)->AddItem(str);
}

void DialogOne::Read()
{
	auto cm = pDialogOne->GetControlManager();
	string str = cm->GetControl<CXListBox>(ID_LB_LIST)->GetSelectedText();
	if (str.empty())
		return;
	cm->GetControl<CXEdit>(ID_EDIT1)->SetText(str);
}

void DialogOne::Remove()
{
	auto cm = pDialogOne->GetControlManager();
	auto pLb = cm->GetControl<CXListBox>(ID_LB_LIST);
	pLb->RemoveItem(pLb->GetSelectedIndex());
}

void DialogOne::Clear()
{
	auto cm = pDialogOne->GetControlManager();
	cm->GetControl<CXListBox>(ID_LB_LIST)->ClearList();
}