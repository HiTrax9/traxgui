#pragma once
#include "stdafx.h"
#include "DialogOne.h"

#define ID_BTNSHOWDLG		0x1000
#define ID_MENU_FILE		0x2000
#define ID_MENU_FILE_OPEN	0x2001
#define ID_MENU_FILE_SAVE	0x2002
#define ID_MENU_FILE_EXIT	0x2003
#define ID_MENU_EDIT		0x3000
#define ID_MENU_EDIT_CUT	0x3001
#define ID_MENU_EDIT_COPY	0x3002
#define ID_MENU_EDIT_PASTE	0x3003
#define ID_MENU_VIEW		0x4000
#define ID_MENU_VIEW_WE		0x4001

class CMainWindow : public CXMainWindow
{
public:
	CMainWindow(HINSTANCE hInstance, string szTitle);
	~CMainWindow();
	int CreateControls();
	void ShowDialog();
	void CreateMenus();

private:
	PXDialog dlgWindow;
};
