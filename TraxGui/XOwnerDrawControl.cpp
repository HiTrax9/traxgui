#include "XOwnerDrawControl.h"

CXOwnerDrawControl::CXOwnerDrawControl()
{
}

CXOwnerDrawControl::~CXOwnerDrawControl()
{
}

HRESULT CXOwnerDrawControl::OnCtlColor(WPARAM wParam, LPARAM lParam)
{
	HDC hdc = (HDC)wParam;
	::SetTextColor(hdc, colText->GetColor());
	return (HRESULT)(LRESULT)GetStockObject(HOLLOW_BRUSH);
}

HRESULT CXOwnerDrawControl::OnDrawItem(WPARAM wParam, LPARAM lParam)
{
	if (wParam && lParam)
	{
		LPDRAWITEMSTRUCT lpDI = (LPDRAWITEMSTRUCT)lParam;
		HTHEME hTheme = OpenThemeData(hWnd, WC_BUTTON);
		Draw(hTheme, lpDI);
	}
	return 0;
}

void CXOwnerDrawControl::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI)
{
	HFONT font = (HFONT)SendMessage(hWnd, WM_GETFONT, 0, 0);
	FillRect(lpDI->hDC, &GetCRect(), pOwner->GetBGColor()->GetBrush());

	GetClientRect(hWnd, &rcBG);
	DetermineTypeAndState();

	GetThemeBackgroundContentRect(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &rcText);
	AdjustRects(lpDI->hDC);

	DTBGOPTS dtbgOpts{ 0 };
	dtbgOpts.dwSize = sizeof(dtbgOpts);
	dtbgOpts.dwFlags |= DTBG_VALIDBITS;
	dtbgOpts.rcClip = rcBG;

	SelectBrush(lpDI->hDC, colBG->GetBrush());

	DrawThemeBackgroundEx(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &dtbgOpts);
	GetThemeBackgroundContentRect(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &GetCRect());

	DTTOPTS dtOpts{ 0 };
	dtOpts.dwSize = sizeof(dtOpts);

	COLORREF clDisabled = 0;
	if (bDisabled)
	{
		clDisabled = colText->GetColor();
		for (int x = 0; x < sizeof(COLORREF); x++)
		{
			unsigned char* pChar = ((unsigned char*)&clDisabled);
			unsigned char c = pChar[x];
			pChar[x] -= c / 4;
			((unsigned char*)&clDisabled)[x] = pChar[x];
		}
	}

	dtOpts.crText = bDisabled ? clDisabled : colText->GetColor();
	dtOpts.dwFlags |= DTT_TEXTCOLOR;
	//HBRUSH hbrush = GetStockBrush(WHITE_BRUSH);
	FillRect(lpDI->hDC, &rcText, pOwner->GetBGColor()->GetBrush());
	DWORD dwDT = 0;
	if (controlType == EGROUPBOX)
		dwDT = DT_CENTER;
	DrawThemeTextEx(hTheme, lpDI->hDC, iControlPart, dwState, szText.c_str(), (int)szText.length(), dwDT, &rcText, &dtOpts);

	if (controlType != EGROUPBOX)
	{
		if (GetFocus() == GetHandle())
			DrawFocusRect(lpDI->hDC, &GetCRect());
	}

	PostDraw();
}