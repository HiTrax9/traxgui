#include "XDialog.h"

bool CXDialog::bRegistered = false;

CXDialog::CXDialog(HINSTANCE hInstance, WNDPROC WndProc, string szTitle)
	: CXMessageHandlerWindow(hInstance, WndProc)
{
	SetText(szTitle);
	SetStyle(WS_XDIALOG);
	SetClass(_T(XDIALOGCLASS));
	SetDimensions(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
	SetPos(DEF_X_POS, DEF_Y_POS);
	SetFont(0);
}

CXDialog::CXDialog(PXWindow& pOwner, string szTitle)
	: CXMessageHandlerWindow(pOwner->GetInstance(), pOwner->GetWndProc())
{
	SetOwner(pOwner);
	SetText(szTitle);
	SetStyle(WS_XDIALOG);
	SetClass(_T(XDIALOGCLASS));
	SetDimensions(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
	SetPos(DEF_X_POS, DEF_Y_POS);
	SetBgColor(pOwner->GetBGColor());
	SetTextColor(pOwner->GetFontColor()->GetColor());
}

CXDialog::~CXDialog()
{
}

int CXDialog::Create()
{
	if (!bRegistered)
	{
		bRegistered = true;
		return CXMessageHandlerWindow::Create(true);
	}
	return CXMessageHandlerWindow::Create();
}

void CXDialog::Show()
{
	if (!hWnd)
	{
		Create();
	}
	ShowWindow(hWnd, SW_SHOW);
}

void CXDialog::Destroy()
{
	//DestroyWindow(hWnd);
	ShowWindow(hWnd, SW_HIDE);
}

int CXDialog::PostCreateControls()
{
	//auto cm = GetControlManager();
	//HWND hLast = GetHandle();
	//UINT uFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW;
	//for (auto p : cm->GetControls())
	//{
	//	SetWindowPos(p.second->GetHandle(), hLast, 0, 0, 0, 0, uFlags);
	//	hLast = p.second->GetHandle();
	//}
	//return S_OK;
	return ReverseTabOrder();
}