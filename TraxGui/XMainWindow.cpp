#include "XMainWindow.h"

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_SETCURSOR:
		{
			auto pWnd = CXWindow::GetXWindow((HWND)wParam);
			if (pWnd)
				if (pWnd->OnSetCursor(wParam, lParam))
					return TRUE;
			break;
		}
		case WM_MOUSELEAVE:
		{
			auto pWnd = CXWindow::GetXWindow(hWnd);
			pWnd->OnMouseLeave(wParam, lParam);
			break;
		}
		case WM_HOTKEY:
		{
			//auto wp = wParam;
			//auto lp = lParam;
			//MsgBox(L"Hotkey test", L"test");
			//need to implement actual shtuff
			break;
		}
		case WM_CONTEXTMENU:
		{
			auto pWnd = CXWindow::GetXWindow((HWND)wParam);
			if (pWnd)
				return pWnd->OnContextMenu(wParam, lParam);
			break;
		}
		case WM_COMMAND:
		{
			auto pCon = CXControl::Get(LOWORD(wParam));
			if (pCon)
				return pCon->OnCommand(wParam, lParam);

			auto pWnd = CXWindow::GetXWindow(hWnd);
			if (pWnd)
			{
				if (pWnd->HasMenu())
				{
					pWnd->MenuHandler(wParam);
				}
			}

			return 0;
		}
		case WM_NOTIFY:
		{
			auto pCtrl = CXWindow::GetXWindow(((LPNMHDR)lParam)->hwndFrom);
			if (pCtrl)
				pCtrl->OnNotify(wParam, lParam);
			break;
		}
		case WM_SIZE:
		case WM_SIZING:
		{
			//auto pWnd = CXMainWindow::Get();
			//if (!pWnd)
			//	break;
			//RECT rcMainWnd{ 0 };
			for (auto c : CXControl::xAnchoredControls)
			{
				c.second->Resize();
			}
			break;
		}
		case WM_PAINT:
		{
			auto pWnd = CXMainWindow::Get();
			if (pWnd)
			{
				PAINTSTRUCT ps;
				HDC hdc = BeginPaint(hWnd, &ps);
				//pWnd->GetBGColor();
				//FillRect(hdc, &pWnd->GetCRect(), pWnd->GetBGColor().GetBrush());
				EndPaint(hWnd, &ps);
			}
			break;
		}
		case WM_CTLCOLOR:
		case WM_CTLCOLORBTN:
		case WM_CTLCOLORSTATIC:
		{
			auto pWnd = CXWindow::GetXWindow((HWND)lParam);
			if (pWnd)
				return pWnd->OnCtlColor(wParam, lParam);
		}
		case WM_DRAWITEM:
		{
			if (CXControl::Get(wParam))
				CXControl::Get(wParam)->OnDrawItem(wParam, lParam);
			return TRUE;
		}
		case WM_SETFOCUS:
		{
			break;
		}
		case WM_KILLFOCUS:
		{
			break;
		}
		case WM_DESTROY:
		case WM_CLOSE:
		{
			CXWindow::GetXWindow(hWnd)->Destroy();
			return 0;
		}
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

PXMainWindow CXMainWindow::pMainWindow;
PXMainWindow CXMainWindow::Get()
{
	return pMainWindow;
}

CXMainWindow::CXMainWindow(HINSTANCE hInstance, string szTitle)
	: CXMessageHandlerWindow(hInstance, MainWndProc)
{
	SetText(szTitle);
	Initialize(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);
}

CXMainWindow::CXMainWindow(HINSTANCE hInstance, string szTitle, uint w, uint h)
	: CXMessageHandlerWindow(hInstance, MainWndProc)
{
	SetText(szTitle);
	Initialize(w, h);
}

CXMainWindow::~CXMainWindow()
{
}

void CXMainWindow::Initialize(uint w, uint h)
{
	SetClass(_T(MAINWNDCLASS));
	SetStyle(MAINWNDSTYLE0);
	SetDimensions(w, h);
	SetPos(DEF_X_POS, DEF_Y_POS);
	SetWndProc(MainWndProc);
	CXFontManager::AddFont(0, L"Segoe UI", 9);
	this->SetFont(0);
	//if (!hCursor)
	//	hCursor = LoadCursor(NULL, IDC_ARROW);
}

int CXMainWindow::Create(bool bRegister)
{
	if (!pMainWindow)
	{
		if (CXMessageHandlerWindow::Create(true) != S_OK) // it's just clearer...
		{
			return S_FALSE;
		}
		pMainWindow = GetPointer()->_dpc<CXMainWindow>();
	}
	return S_OK;
}

void CXMainWindow::Destroy()
{
	DestroyWindow(hWnd);
	PostQuitMessage(0);
}