#pragma once
#include "XControlManager.h"

class CXMessageHandlerWindow;

typedef PXWindow PXMsgHandler;
typedef std::vector<PXMsgHandler> VXMsgHandlers;

class CXMessageHandlerWindow : public CXControlMgrWindow
{
	static VXMsgHandlers vXMsgHandlers;
public:
	static VXMsgHandlers& Handlers();

public:
	CXMessageHandlerWindow(HINSTANCE hInstance, WNDPROC WndProc);
	~CXMessageHandlerWindow();
	int Create(bool bRegistered = false);
	void AddMsgHandlerWnd(PXWindow pWindow);
};
