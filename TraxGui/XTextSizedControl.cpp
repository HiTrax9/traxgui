#include "XTextSizedControl.h"

CXTextSizedControl::CXTextSizedControl()
{
}

CXTextSizedControl::~CXTextSizedControl()
{
}

int CXTextSizedControl::Create(bool bRegister)
{
	return CXControl::Create(bRegister);
}

RECT CXTextSizedControl::GetTextRect()
{
	return GetTextRect(szText);
}

RECT CXTextSizedControl::GetTextRect(string szText)
{
	return GetTextRect(GetWndDC(), szText);
}

RECT CXTextSizedControl::GetTextRect(HDC hdc, string szText)
{
	auto pFont = GetFont();
	if (pFont)
		SelectObject(hdc, GetFont()->Get());
	else
	{
		int id = pFont->GetID();
	}
	TEXTMETRIC tm{ 0 };
	if (!GetTextMetrics(hdc, &tm))
		return RECT{ 0 };

	SIZE size;
	if (!GetTextExtentPoint32(hdc, szText.c_str(), (int)szText.size(), &size))
		return RECT{ 0 };

	int h = tm.tmHeight + iPadding;
	int w = tm.tmAveCharWidth * (int)szText.size() + iPadding;

	w = size.cx;
	h = size.cy;

	RECT rc = { 0,0,w,h };
	return rc;
}

void CXTextSizedControl::SetDimensions(uint w, uint h, bool bUpdate)
{
	if (!w || !h)
	{
		HDC hdc = pOwner->GetWndDC();

		if (!bHasFont)
			SetFont(pOwner->GetFont()->GetID());

		SelectObject(hdc, GetFont()->Get());
		RECT rc = GetTextRect(hdc, szText);

		if (!uWidth)
			uWidth = (w) ? w : rc.right;
		if (!uHeight)
			uHeight = (h) ? h : rc.bottom;
	}
	else
	{
		CXControl::SetDimensions(w, h, false);
	}
	CXControl::SetDimensions(uWidth, uHeight, bUpdate);
}