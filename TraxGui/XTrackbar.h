#pragma once
#include "XFunctionalControl.h"

#define WS_XTRACKBAR WS_CHILD | WS_VISIBLE | TBS_AUTOTICKS | TBS_ENABLESELRANGE

class CXTrackbar :
	public CXFunctionalControl
{
public:
	CXTrackbar();
	~CXTrackbar();
};

#define Trackbar_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ETRACKBAR)