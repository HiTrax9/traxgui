#include "XCommandLink.h"

CXCommandLink::CXCommandLink()
{
	SetClass(WC_BUTTON);
	SetStyle(WS_XCOMMANDLINK);
}

CXCommandLink::~CXCommandLink()
{
}

void CXCommandLink::SetNote(string szText)
{
	Button_SetNote(hWnd, szText.c_str());
}