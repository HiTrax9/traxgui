#pragma once
#ifndef XCM
#include "XControlManager.h"
#endif // !XCM
#include "XButton.h"
#include "XCombobox.h"
class CXControlManager;
typedef std::shared_ptr<CXControlManager> PXControlManager;

class CXGroupbox; //this fucker is a bit of a special case...
typedef std::shared_ptr<CXGroupbox> PXGroupbox;
// i feel like this might be the magic sauce right V here...
#define WS_XGROUPBOX WS_VISIBLE | WS_CHILD | SS_OWNERDRAW

class CXGroupbox : public CXMouseTrackingControl
{
	PXControlManager xCtrlMgr;
public:
	CXGroupbox();
	~CXGroupbox();
	int Create(bool bRegister = false);
	PXControlManager GetControlManager();
	virtual void DetermineTypeAndState();
	virtual void AdjustRects(HDC hdc);
	void PostDraw();
public:
	PXButton AddButton(XControlProperties& cp);
	PXEdit AddEdit(XControlProperties& cp);
	PXLabel AddLabel(XControlProperties& cp);
	PXListBox AddListBox(XControlProperties& cp);
	PXListView AddListView(XControlProperties& cp);
	PXIcon AddIcon(XControlProperties& cp);
	PXBitmap AddBitmap(XControlProperties& cp);
	PXCheckbox AddCheckbox(XControlProperties& cp);
	PXRadio AddRadio(XControlProperties& cp);
	PXGroupbox AddGroupbox(XControlProperties& cp);
	PXCombobox AddCombobox(XControlProperties& cp);
	template<class T>
	std::shared_ptr<T> AddControl(XControlProperties & cp);
	void AddControl(PXControl pControl);

private:
	void AdjustToBox(PXControl pControl);

private:
	std::vector<PXControl> pControls;
	HPEN hBorderPen = 0;
	COLORREF clBorder = RGB(225, 225, 225);
};

#define Groupbox_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, EGROUPBOX)

template<class T>
inline std::shared_ptr<T> CXGroupbox::AddControl(XControlProperties & cp)
{
	PXControl pCtrl = xCtrlMgr->CreateControl<T>(cp);
	AdjustToBox(pCtrl);
	pCtrl->Anchor(eAnkhDir);
	this->pControls.push_back(pCtrl);
	return pCtrl->_dpc<T>();
}
