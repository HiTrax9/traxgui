#pragma once
#include "XStateControl.h"

#define WS_XCHECKBOX WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_OWNERDRAW

class CXCheckbox : public CXStateControl
{
public:
	CXCheckbox();
	~CXCheckbox();
	//void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);
private:
};

typedef std::shared_ptr<CXCheckbox> PXCheckbox;

#define Checkbox_MakeCP(id, x, y, s) CXControl::MakeCP(id, x, y, 0, 0, s, ECHECKBOX)