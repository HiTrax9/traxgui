#include "XColor.h"

std::map<COLORREF, PXColor> CXColor::xColors;

PXColor CXColor::Make(COLORREF clColor)
{
	if (xColors[clColor])
		return xColors[clColor]->shared_from_this();

	xColors[clColor] = std::make_shared<CXColor>(clColor);
	return xColors[clColor]->shared_from_this();
}

PXColor CXColor::Make(HBRUSH hBrush)
{
	LOGBRUSH lb{ 0 };
	GetObject(hBrush, sizeof(lb), &lb);
	return Make(lb.lbColor);
}

PXColor CXColor::Get(COLORREF clColor)
{
	return Make(clColor);
}

void CXColor::Purge()
{
	std::vector<COLORREF> vDeleteme;
	for (auto p : xColors)
	{
		//ref_count should be two,
		//reason: one ref in the static map
		//		another ref in this loop.
		if (p.second.use_count() == 2)
		{
			//auto ptr = p.second;
			//int i = ptr.use_count();
			////xColors.erase(p.second->GetColor());
			//p.second.reset();
			vDeleteme.push_back(p.second->GetColor());
		}
	}
	for (auto c : vDeleteme)
		xColors.erase(c);
}

CXColor::CXColor()
{
	this->clColor = 0;
}

CXColor::CXColor(COLORREF clColor)
{
	this->clColor = clColor;
}

CXColor::CXColor(HBRUSH hBrush)
{
	LOGBRUSH lb{ 0 };

	this->hBrush = hBrush;
	GetObject(hBrush, sizeof(lb), &lb);
	this->clColor = lb.lbColor;
}

CXColor::~CXColor()
{
	Free();
}

HPEN CXColor::GetPen(int psStyle, int iWidth)
{
	if (!hPen)
		hPen = CreatePen(PS_SOLID, iWidth, clColor);

	return hPen;
}

HBRUSH CXColor::GetBrush()
{
	if (!hBrush)
		hBrush = CreateSolidBrush(clColor);
	return hBrush;
}

COLORREF CXColor::GetColor()
{
	return clColor;
}

void CXColor::Free()
{
	if (hPen)
	{
		DeleteObject(hPen);
		hPen = 0;
	}
	if (hBrush)
	{
		DeleteObject(hBrush);
		hBrush = 0;
	}
}