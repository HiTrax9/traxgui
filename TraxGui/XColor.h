#pragma once
#include "stdafx.h"

class CXColor;
typedef std::shared_ptr<CXColor> PXColor;

class CXColor : public std::enable_shared_from_this<CXColor>
{
public:
	static std::map<COLORREF, PXColor> xColors;
	static PXColor Make(COLORREF clColor);
	static PXColor Make(HBRUSH hBrush);
	static PXColor Get(COLORREF clColor); // kinda just for semantics
	static void Purge();//destroys any unused colors (when ref count == 1, the map xColors would be the last owner)
public:
	CXColor();
	CXColor(COLORREF clColor);
	CXColor(HBRUSH hBrush);
	~CXColor();

	HPEN GetPen(int psStyle, int iWidth);
	HBRUSH GetBrush();
	COLORREF GetColor();

	CXColor& operator=(const COLORREF & rhs)
	{
		Free();
		this->clColor = rhs;
		return *this;
	}

private:
	// cleans up gdi objects
	void Free();

private:
	COLORREF clColor = 0;
	HBRUSH hBrush = 0;
	HPEN hPen = 0;
};
