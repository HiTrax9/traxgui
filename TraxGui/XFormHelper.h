#pragma once
#include "XControlManager.h"

class CXFormHelper :
	public std::enable_shared_from_this<CXFormHelper>
{
public:
	CXFormHelper();
	~CXFormHelper();

private:
	PXControlManager pCM;
};
