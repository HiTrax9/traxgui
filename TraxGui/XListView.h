#pragma once
#include "XCollectionControl.h"

#define WS_XLISTVIEW WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | LVS_REPORT
#define WS_XLISTVIEW_ WS_XLISTVIEW
class CXListViewItem
{
	int iItem = 0, iSubItem = 0;
	tstring szText;
	LVITEM lvi{ 0 };
	HICON hIcon = NULL;
	CXListViewItem * xSubItem = nullptr;
	std::vector<CXListViewItem*> vSubItems;

public:
	CXListViewItem() = default;
	CXListViewItem(int iItem, int iSubItem, tstring szText);
	CXListViewItem(int iItem, int iSubItem, tstring szText, HICON hIcon);
	~CXListViewItem();

	LVITEM GetItem();
	std::vector<CXListViewItem*> GetSubItems();
	void AddSubItem(int iItem, int iSubItem, tstring szText);
	CXListViewItem* GetSubItem(int iSubItem);
	int GetItemIndex();
	void SetText(tstring szText);
	tstring GetText();
	HICON GetIconHandle();
};

class CXListView : public CXFunctionalControl
{
public:
	CXListView();
	~CXListView();
	void SetStyleEx(DWORD dwStyleEx);
	//virtual bool Create();
	//virtual void OnNotify(UINT code, LPARAM lParam);
	bool InsertColumn(tstring szText);
	bool InsertColumn(tstring szText, int width);
	bool InsertColumn(tstring szText, int width, DWORD lvcfFmt);
	void SetColumnWidth(int iColumn, int width);
	bool AddItem(int iItem, int iSubItem);
	bool AddItem(CXListViewItem & xListViewItem);
	int GetSelectedIndex();
	void SetIconList();
	void Clear();
private:
	int iCol = 0;
	std::map<int, CXListViewItem> mListItems;
	HIMAGELIST hSmall;
};

typedef std::shared_ptr<CXListView> PXListView;

#define ListView_MakeCP(id, x, y, w, h) CXControl::MakeCP(id, x, y, w, h, _T(""), ELISTVIEW)