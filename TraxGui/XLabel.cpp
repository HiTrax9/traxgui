#include "XLabel.h"

LRESULT CALLBACK LabelCallback(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
{
	switch (uMsg)
	{
		case WM_MOUSELEAVE:
		case WM_NCMOUSELEAVE:
		{
			if (dwRefData)
			{//return reinterpret_cast<CXLabel*>(dwRefData)->OnDrawItem(wParam, lParam);
				recast<CXLabel*>(dwRefData)->OnMouseLeave(wParam, lParam);
			}
			break;
		}
		//case WM_MOUSEHOVER:
		case WM_MOUSEMOVE:
		{
			if (dwRefData)
			{
				recast<CXLabel*>(dwRefData)->OnMouseMove(wParam, lParam); // oh...
			}
			break;
		}
		case WM_DRAWITEM:
		{
			if (dwRefData)
			{
				return recast<CXLabel*>(dwRefData)->OnDrawItem(wParam, lParam);
			}

			break;
		}
		case WM_NCHITTEST:
		{
			if (dwRefData)
			{//return reinterpret_cast<CXLabel*>(dwRefData)->OnDrawItem(wParam, lParam);
				//reinterpret_cast<CXLabel*>(dwRefData)->SetText(L"Hit!");
			}
			break;
		}
	}
	return DefSubclassProc(hWnd, uMsg, wParam, lParam);
}

CXLabel::CXLabel()
{
	SetClass(_T("STATIC"));
	SetStyle(WS_XLABEL);
}

CXLabel::~CXLabel()
{
}

//int CXLabel::Create(bool bRegister)
//{
//	if (CXOwnerDrawControl::Create(bRegister) != S_OK)
//		return S_FALSE;
//	return SetWindowSubclass(hWnd, LabelCallback, 1, (DWORD_PTR)this);
//}

XControlProperties& CXLabel::TransformCP(XControlProperties & cp)
{
	cp.controlType = ELABEL;
	return cp;
}

void CXLabel::SetText(string szText)
{
	if (!this->szText.empty())
	{
		szOldText = this->szText;
		CXWindow::SetText(szText);
		Clear();
		Redraw();
	}
	else
		CXWindow::SetText(szText);
	Redraw();
}

void CXLabel::SetText(string szText, PXColor colText)
{
	this->colText = colText;
	SetText(szText);
}

void CXLabel::SetTransparentBG(bool bTransparentBG)
{
	this->bTransparentBG = bTransparentBG;
	Redraw();
}

void CXLabel::SetFont(FontID fontID)
{
	Clear();
	CXWindow::SetFont(fontID);
	RECT rc = GetTextRect();
	SetDimensions(rc.right, rc.bottom);
}

bool bErased = false;
void CXLabel::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI)
{
	Clear();
	HDC hdc = lpDI->hDC;
	SelectObject(hdc, GetFont()->Get());
	int fontId = GetFont()->GetID();

	if (!bErased && !szOldText.empty())
	{
		Redraw();
		bErased = true;
	}
	else
	{
		RECT rc = GetTextRect(hdc, szText);
		SetDimensions(rc.right, rc.bottom);

		if (bTransparentBG)
			SetBkMode(hdc, TRANSPARENT);
		else
		{
			SetBkMode(hdc, TRANSPARENT);
			FillRect(hdc, &rc, colBG->GetBrush());
		}
		::SetTextColor(hdc, colText->GetColor());
		DrawText(hdc, szText.c_str(), (int)szText.length(), &rc, DT_LEFT);
		bErased = false;
	}
}