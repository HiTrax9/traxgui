#include "Error.h"

namespace Error
{
	cstring GetLastErrorMsg()
	{
		cstring buf;
		buf.s.resize(256);
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			&buf.s[0], (DWORD)buf.s.size(), NULL);

		return buf;
	}
	void Box(string szMsg)
	{
		_EMsgBox(szMsg.c_str());
	}
	void MsgBoxGLE()
	{
		_EMsgBox(GetLastErrorMsg().c_str());
	}
	void MsgBoxSGLE(cstring szMsg)
	{
		szMsg += "\n";
		szMsg += GetLastErrorMsg();
		_EMsgBox(szMsg.c_str());
	}
}