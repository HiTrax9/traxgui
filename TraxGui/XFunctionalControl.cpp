#include "XFunctionalControl.h"

CXFunctionalControl::CXFunctionalControl()
{
}

CXFunctionalControl::~CXFunctionalControl()
{
}

void CXFunctionalControl::SetFunction(XFnID xFnID, FXFunction pFunc, void * pData)
{
	fnMap[xFnID] = FXControlFunction(pFunc, pData);
}

void CXFunctionalControl::SetFunction(XFnID xFnID, FXControlCallback pFunc, void * pData)
{
	fnMap[xFnID] = FXControlFunction(pFunc, pData);
}

int CXFunctionalControl::CallFunction(XFnID xFnID)
{
	if (fnMap[xFnID].pFunc)
		return fnMap[xFnID].Call();
	return 0;
}

int CXFunctionalControl::CallFunction(XFnID xFnID, void * pData)
{
	if (fnMap[xFnID].pFunc)
		return fnMap[xFnID].Call(pData);
	return 0;
}

void CXFunctionalControl::SetEventHandler(EVENT_ID eID, FXControlCallback pFunc, void * pData)
{
	SetFunction(eID, pFunc, pData);
}

void CXFunctionalControl::SetEventHandler(EVENT_ID eID, FXFunction pFunc, void * pData)
{
	SetFunction(eID, pFunc, pData);
}

void CXFunctionalControl::SetEventHandler(EVENT_ID eID, FXFunction pFunc, int pData)
{
	SetFunction(eID, pFunc, recast<void*>((uintptr_t)pData));
}

HRESULT CXFunctionalControl::OnNotify(WPARAM wParam, LPARAM lParam)
{
	return CallFunction(SETCURSOR_EID);
}

HRESULT CXFunctionalControl::OnSetCursor(WPARAM wParam, LPARAM lParam)
{
	//if (hCursor)
	//{
	//	hCursorOld = SetCursor(hCursor);
	//	return TRUE;
	//}
	return CallFunction(SETCURSOR_EID);
}

HRESULT CXFunctionalControl::OnMouseMove(WPARAM wParam, LPARAM lParam)
{
	return CallFunction(MOUSEMOVE_EID);
}

HRESULT CXFunctionalControl::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	return CallFunction(MOUSELEAVE_EID);
}

HRESULT CXFunctionalControl::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int id = HIWORD(wParam);
	switch (id)
	{
		case BN_CLICKED:
		{
			CallFunction(CLICK_EID);
			break;
		}
		case BN_DBLCLK:
		{
			CallFunction(DBLCLICK_EID);
			break;
		}
	}
	return 0;
}