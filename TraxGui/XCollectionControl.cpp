#include "XCollectionControl.h"

CXCollectionControl::CXCollectionControl()
{
}

CXCollectionControl::~CXCollectionControl()
{
}

void CXCollectionControl::AddItem(CXItem xItem)
{
	vItems.push_back(xItem);
}

void CXCollectionControl::AddItem(string szText)
{
	vItems.push_back(CXItem(szText));
}

void CXCollectionControl::AddItem(string szText, void* pItemData)
{
	vItems.push_back(CXItem(szText, pItemData));
}

CXItem CXCollectionControl::GetItem(uint uIndex)
{
	if (uIndex < vItems.size())
		return vItems[uIndex];
	return NULL;
}

string CXCollectionControl::GetItemText(uint uIndex)
{
	if (uIndex < vItems.size())
		return vItems[uIndex].GetText();

	return _T("");
}

void* CXCollectionControl::GetItemDataPtr(uint uIndex)
{
	if (uIndex < vItems.size())
		return vItems[uIndex].GetDataPtr();
	return nullptr;
}

void CXCollectionControl::RemoveItem(uint uIndex)
{
	if (uIndex < vItems.size())
		vItems.erase(vItems.begin() + uIndex);
}