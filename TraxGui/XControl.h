#pragma once
#include "XWindow.h"

enum ECONTROLTYPE
{
	EINVALID = -1,
	EBUTTON,
	EEDIT,
	ELABEL,
	ELISTBOX,
	ELISTVIEW,
	EICON,
	EBITMAP,
	ECHECKBOX,
	ERADIO,
	EGROUPBOX,
	ETRACKBAR,
	ECOMBOBOX,
	ECOMMANDLINK,
	ESYSLINK
};

struct XControlProperties
{
	XID xUserID;
	int x;
	int y;
	uint w;
	uint h;
	HICON hIcon;
	HICON hIconSm;
	HCURSOR hCursor;
	string szText;
	ECONTROLTYPE controlType;
};

class CXControl;
typedef std::shared_ptr<CXControl> PXControl;
typedef std::map<XID, PXControl> XControlMap;

enum EANCHORDIR
{
	NONE,
	RIGHT,
	LR,
	BOTTOM,
	TB,
	INPLACE
};

class CXControl : public CXWindow
{
public:
	static XControlMap xAppControls;
	static XControlMap xAnchoredControls;

	// not to be used by mere mortals
	// internal use only... i mean,.. i guess..
	static PXControl Get(XID id);
public:
	template <class T>
	static std::shared_ptr<T> Make(PXWindow& pOwner, XControlProperties& cp);
	static XControlProperties MakeCP(XID xUserID, int x, int y, uint w, uint h);
	static XControlProperties MakeCP(XID xUserID, int x, int y, uint w, uint h, string szText);
	static XControlProperties MakeCP(XID xUserID, int x, int y, uint w, uint h, string szText, ECONTROLTYPE controlType);

public:
	virtual ECONTROLTYPE GetControlType();
	virtual XID GetUserID();
	virtual void Anchor(EANCHORDIR eAnkhDir);
	virtual void Resize();
	virtual void Disable(bool b);
protected:
	virtual void SetControlType(ECONTROLTYPE controlType);
	virtual void SetUserID(XID xUserID);

protected:
	ECONTROLTYPE controlType = EINVALID;
	EANCHORDIR eAnkhDir = NONE;
	DWORD dwAnkDir = 0;
	XID xUserID = 0;
	bool bDisabled = false;
	SIZE anchorDeltaX{ 0 };
	SIZE anchorDeltaY{ 0 };
	SIZE orgSize{ 0 };
	POINT orgPos{ 0 };
};

#define _MakeCP(x, y) CXControl::MakeCP(0,x,y,0,0)
#define _MakeCP_(x, y, w, h) CXControl::MakeCP(0,x,y,w,h)
#define _MakeCP_S(x, y, w, h, s) CXControl::MakeCP(0,x,y,w,h,s)
#define _MakeCP_S_(x, y, s) CXControl::MakeCP(0,x,y,0,0,s)

template<class T>
inline std::shared_ptr<T> CXControl::Make(PXWindow & pOwner, XControlProperties & cp)
{
	assert(cp.controlType > EINVALID);
	static PXControl pControl;

	switch (cp.controlType)
	{
		case EBUTTON:
		{
			pControl = std::make_shared<CXButton>();
			pControl->SetControlType(EBUTTON);
			break;
		}
		case EEDIT:
		{
			pControl = std::make_shared<CXEdit>();
			pControl->SetControlType(EEDIT);
			break;
		}
		case ELABEL:
		{
			pControl = std::make_shared<CXLabel>();
			pControl->SetControlType(ELABEL);
			break;
		}
		case ELISTBOX:
		{
			pControl = std::make_shared<CXListBox>();
			pControl->SetControlType(ELISTBOX);
			break;
		}
		case ELISTVIEW:
		{
			pControl = std::make_shared<CXListView>();
			pControl->SetControlType(ELISTVIEW);
			break;
		}
		case EICON:
		{
			pControl = std::make_shared<CXIcon>();
			pControl->SetControlType(EICON);
			break;
		}
		case EBITMAP:
		{
			pControl = std::make_shared<CXBitmap>();
			pControl->SetControlType(EBITMAP);
			break;
		}
		case ECHECKBOX:
		{
			pControl = std::make_shared<CXCheckbox>();
			pControl->SetControlType(ECHECKBOX);
			break;
		}
		case ERADIO:
		{
			pControl = std::make_shared<CXRadio>();
			pControl->SetControlType(ERADIO);
			break;
		}
		case EGROUPBOX:
		{
			pControl = std::make_shared<CXGroupbox>();
			pControl->SetControlType(EGROUPBOX);
			break;
		}
		case ETRACKBAR:
		{
			pControl = std::make_shared<CXTrackbar>();
			pControl->SetControlType(ETRACKBAR);
			break;
		}
		case ECOMBOBOX:
		{
			pControl = std::make_shared<CXCombobox>();
			pControl->SetControlType(ECOMBOBOX);
			break;
		}
		case ECOMMANDLINK:
		{
			pControl = std::make_shared<CXCommandLink>();
			pControl->SetControlType(ECOMMANDLINK);
			break;
		}
		case ESYSLINK:
		{
			pControl = std::make_shared<CXSysLink>();
			pControl->SetControlType(ESYSLINK);
			break;
		}
	}

	pControl->SetInstance(pOwner->GetInstance());
	pControl->SetWndProc(pOwner->GetWndProc());
	pControl->SetOwner(pOwner);

	pControl->SetID(xAppControls.size() + 1);
	xAppControls[pControl->GetID()] = pControl->GetPointer()->_dpc<CXControl>();
	if (!cp.xUserID)
		cp.xUserID = pControl->GetID();
	pControl->SetUserID(cp.xUserID);

	pControl->SetBgColor(pOwner->GetBGColor());
	if (pOwner->GetFont())
		pControl->SetFont(pOwner->GetFont()->GetID());
	pControl->SetTextColor(pOwner->GetFontColor());

	if (cp.hCursor)
		pControl->SetCursor(cp.hCursor);

	if (!cp.szText.empty())
		pControl->SetText(cp.szText);

	pControl->SetPos(cp.x, cp.y);
	pControl->SetDimensions(cp.w, cp.h);
	pControl->Disable(false);

	return pControl->_dpc<T>();
}
