#pragma once
#include "XTextSizedControl.h"

#define BS_XBUTTON (WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | BS_TEXT | BS_CENTER)
#define BS_XBUTTONICON (WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_TEXT | BS_ICON | BS_PUSHBUTTON | BS_CENTER)
#define BS_DEFXBUTTON (WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | BS_CENTER)

class CXButton : public CXTextSizedControl
{
public:
	CXButton();
	~CXButton();
	static XControlProperties& TransformCP(XControlProperties& cp);

	virtual int Create(bool bRegister = false);
	virtual void Disable(bool b);
	virtual void SetIcon(HANDLE hIcon);
	virtual void SetBitmap(HANDLE hBitmap);
public: // message handlers
	HRESULT OnCommand(WPARAM wParam, LPARAM lParam);
};

typedef std::shared_ptr<CXButton> PXButton;

#define Button_MakeCP(id, x, y, w, h, s) CXButton::TransformCP(CXControl::MakeCP(id, x, y, w, h, s))
