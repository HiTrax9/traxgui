#pragma once
#include "XOwnerDrawControl.h"

class CXTextSizedControl : public CXOwnerDrawControl
{
public:
	CXTextSizedControl();
	~CXTextSizedControl();
	int Create(bool bRegister = false);
	RECT GetTextRect();
	RECT GetTextRect(string szText);
	RECT GetTextRect(HDC hdc, string szText);
	void SetDimensions(uint w, uint h, bool bUpdate = true);
protected:
	int iPadding = 0;
};
