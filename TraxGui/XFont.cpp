#include "XFont.h"

std::map<FontID, PXFont> CXFontManager::mFonts;

CXFont::CXFont()
{
}

CXFont::CXFont(string szFace, int height, int width)
{
	this->szFace = szFace;
	this->iHeight = height;
	this->iWidth = width;
}

CXFont::~CXFont()
{
	if (hFont)
		DeleteObject(hFont);
}

HFONT CXFont::Create()
{
	int nHeight = (!bPoints) ? iHeight : Size();//-MulDiv(iHeight, GetDeviceCaps(GetDC(GetDesktopWindow()), LOGPIXELSY), 72);
	if (!hFont)
		hFont = CreateFont(
			nHeight,
			iWidth,
			0, 0,
			iWeight,
			bItalic,
			bUnderlined,
			bStriked,
			dwCharset,
			OUT_DEFAULT_PRECIS,
			CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY,
			FF_DONTCARE,
			szFace.c_str());

	return hFont;
}

void CXFont::Reset(string szFace, int height, int width)
{
	Release();
	this->szFace = szFace;
	this->iHeight = height;
	this->iWidth = width;
}

HFONT CXFont::Get()
{
	return hFont;
}

SizePoint CXFont::Size()
{
	return -MulDiv(iHeight, GetDeviceCaps(GetDC(GetDesktopWindow()), LOGPIXELSY), 72);
}

void CXFont::UsePoints(bool bBool)
{
	this->bPoints = bBool;
}

PXFont CXFont::GetPointer()
{
	return shared_from_this();
}

FontID CXFont::GetID()
{
	return id;
}

void CXFont::SetID(FontID id)
{
	this->id = id;
}

void CXFont::Release()
{
	if (hFont)
		DeleteObject(hFont);
	hFont = nullptr;
}

PXFont CXFontManager::GetFont(FontID fontID)
{
	if (mFonts[fontID])
		return mFonts[fontID]->GetPointer();
	return nullptr;
}

PXFont CXFontManager::GetFont(FontID fontID, string szFace)
{
	return GetFont(fontID, szFace, DEFAULT_FONT_SIZE);
}

PXFont CXFontManager::GetFont(FontID fontID, string szFace, SizePoint uSize)
{
	AddFont(fontID, szFace, uSize);
	return mFonts[fontID];
}

HFONT CXFontManager::AddFont(FontID fontID, string szFace, SizePoint uSize)
{
	if (!mFonts[fontID])
	{
		auto pFont = mFonts[fontID] = std::make_shared<CXFont>(szFace, uSize, 0);
		pFont->SetID(fontID);
		return pFont->Create();
	}
	return ReplaceFont(fontID, szFace, uSize);
}

HFONT CXFontManager::ReplaceFont(FontID fontID, string szFace, SizePoint uSize)
{
	auto pFont = mFonts[fontID];
	if (!pFont)
		return HFONT(0);

	pFont->Reset(szFace, uSize);
	return pFont->Create();
}