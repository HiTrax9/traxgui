#include "XMessageHandlerWindow.h"

VXMsgHandlers CXMessageHandlerWindow::vXMsgHandlers;

VXMsgHandlers& CXMessageHandlerWindow::Handlers()
{
	return vXMsgHandlers;
}

CXMessageHandlerWindow::CXMessageHandlerWindow(HINSTANCE hInstance, WNDPROC WndProc)
	: CXControlMgrWindow(hInstance, WndProc)
{
}

CXMessageHandlerWindow::~CXMessageHandlerWindow()
{
}

int CXMessageHandlerWindow::Create(bool bRegistered)
{
	if (CXControlMgrWindow::Create(bRegistered) != S_OK)
		return S_FALSE;

	vXMsgHandlers.push_back(GetPointer());

	return S_OK;
}

void CXMessageHandlerWindow::AddMsgHandlerWnd(PXWindow pWindow)
{
	vXMsgHandlers.push_back(pWindow->GetPointer());
}