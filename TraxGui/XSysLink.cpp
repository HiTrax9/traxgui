#include "XSysLink.h"

CXSysLink::CXSysLink()
{
	SetClass(WC_LINK);
	SetStyle(WS_XSYSLINK);
}

CXSysLink::~CXSysLink()
{
}

void CXSysLink::SetLink(string szLink, bool bUpdate)
{
	this->szLink = szLink;
	if (bUpdate)
	{
		LITEM li{ 0 };
		li.mask = LIF_URL | LIF_STATE;
		li.state = LIS_HOTTRACK | LIS_FOCUSED | LIS_ENABLED;
		li.stateMask = LIS_HOTTRACK | LIS_FOCUSED | LIS_ENABLED;
		lstrcpy(li.szUrl, szLink.c_str());
		SendMessage(hWnd, LM_SETITEM, 0, (LPARAM)&li);
	}
}

HRESULT CXSysLink::OnNotify(WPARAM wParam, LPARAM lParam)
{
	switch ((int)(((LPNMHDR)lParam)->code))
	{
		case NM_CLICK:
		case NM_RETURN:
		{
			PNMLINK pNMLink = (PNMLINK)lParam;
			LITEM lItem = pNMLink->item;

			if ((((LPNMHDR)lParam)->hwndFrom == hWnd) && (lItem.iLink == 0))
			{
				ShellExecute(NULL, _T("open"), lItem.szUrl, NULL, NULL, SW_SHOW);
			}

			break;
		}
	}
	return S_OK;
}