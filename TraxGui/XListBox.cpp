#include "XListBox.h"

#include "XControlManager.h"

CXListBox::CXListBox()
{
	SetClass(_T("LISTBOX"));
	SetStyle(WS_XLISTBOX);
}

CXListBox::~CXListBox()
{
}

int CXListBox::Create(bool bRegister)
{
	if (CXFunctionalControl::Create() != S_OK)
		return S_FALSE;

	AddItem(szText, nullptr);
	return S_OK;
}

void CXListBox::AddItem(string szText, void * pItemData)
{
	if (!szText.empty())
	{
		CXCollectionControl::AddItem(szText, pItemData);
		ListBox_AddString(hWnd, szText.c_str());
	}
}

void CXListBox::RemoveItem(uint uIndex)
{
	if (uIndex > GetItemCount())
		return;

	CXCollectionControl::RemoveItem(uIndex);
	ListBox_DeleteString(hWnd, uIndex);
	if (uIndex >= GetItemCount())
		ListBox_SetCurSel(hWnd, --uIndex);
	else
		ListBox_SetCurSel(hWnd, uIndex);
}

int CXListBox::GetSelectedIndex()
{
	int count = GetItemCount();
	for (int x = 0; x < count; x++)
		if (ItemIsSelected(x))
			return x;

	return -1;
}

string CXListBox::GetSelectedText()
{
	int i = GetSelectedIndex();
	int len = ListBox_GetTextLen(hWnd, i);
	if (len < 0)
		return _T("");
	string str((size_t)len, 0);
	ListBox_GetText(hWnd, i, &str[0]);
	return str.c_str();
}

bool CXListBox::ItemIsSelected(int index)
{
	return (ListBox_GetSel(hWnd, index) > 0);
}

uint CXListBox::GetItemCount()
{
	return ListBox_GetCount(hWnd);
}

void CXListBox::ClearList()
{
	ListBox_ResetContent(hWnd);
}

HRESULT CXListBox::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int id = HIWORD(wParam);
	switch (id)
	{
		case LBN_DBLCLK:
		{
			CallFunction(DBLCLICK_EID);
			break;
		}
		case LBN_SETFOCUS:
		{
			//auto pCon = pOwner->_dpc<CXControlMgrWindow>()->GetControlManager();
			//for (auto p : pCon->GetControls())
			//	SendMessage(p.second->GetHandle(), WM_KILLFOCUS, 0, 0);
			return CallFunction(SETFOCUS_EID);
			break;
		}
		case LBN_KILLFOCUS:
		{
			CallFunction(KILLFOCUS_EID);
			break;
		}
	}
	return 0;
}