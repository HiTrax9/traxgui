#pragma once
#include "XTextSizedControl.h"

#define WS_XEDIT	WS_CHILD | WS_TABSTOP | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_AUTOHSCROLL
#define WS_XEDITML	WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL

#define DEFAULT_EDIT_WIDTH 100

enum EBUDDYPOS
{
	EB_LEFT,
	EB_RIGHT,
	EB_TOPLEFT,
	EB_TOPRIGHT,
	EB_BOTTOMLEFT,
	EB_BOTTOMRIGHT
};

class CXEdit : public CXTextSizedControl
{
public:
	CXEdit();
	~CXEdit();
	//int Create(bool bRegisterClass = false);
public:
	void SetText(string szText);
	string GetText();
	void SetDimensions(uint w, uint h, bool bUpdate = true);
	void SetBuddyLabel(string szText, EBUDDYPOS eBuddyPos, PXControl pGroupbox = nullptr);

private:
	POINT GetBuddyPos(string szText, EBUDDYPOS eBuddyPos);
private:
	XID xBuddyLblID = 0;
};

typedef std::shared_ptr<CXEdit> PXEdit;

#define Edit_MakeCP_(id, x, y, w, s) CXControl::MakeCP(id, x, y, w, 0, s, EEDIT)
#define Edit_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, EEDIT)
