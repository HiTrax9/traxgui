#include "XCombobox.h"

CXCombobox::CXCombobox()
{
	SetClass(WC_COMBOBOX);
	SetStyle(WS_XCOMBOBOX);
}

CXCombobox::~CXCombobox()
{
}

int CXCombobox::Create(bool bRegister)
{
	if (CXTextSizedControl::Create(bRegister) != S_OK)
		return S_FALSE;

	if (!szText.empty())
	{
		AddItem(szText);
		SetSelectedItem();
	}
	return S_OK;
}

void CXCombobox::AddItem(CXItem xItem)
{
	CXCollectionControl::AddItem(xItem);
	ComboBox_AddString(hWnd, xItem.GetText().c_str());
}

void CXCombobox::AddItem(string szText)
{
	CXCollectionControl::AddItem(szText);
	ComboBox_AddString(hWnd, szText.c_str());
}

void CXCombobox::SetSelectedItem(int iItem)
{
	if (iItem < 0)
		ComboBox_SetCurSel(hWnd, 0);
	else
		ComboBox_SetCurSel(hWnd, iItem);
}