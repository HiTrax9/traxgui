#include "XGuiBase.h"

bool CXGuiBase::bInitialized = false;

int CXGuiBase::Initialize()
{
	if (!bInitialized)
	{
		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
		icc.dwICC = ICC_TAB_CLASSES | ICC_LISTVIEW_CLASSES | ICC_LINK_CLASS;
		if (!InitCommonControlsEx(&icc))
			return GetLastError();
	}
	else
		return S_OK;
	bInitialized = true;
	return S_OK;
}