#pragma once
#include "XTextSizedControl.h"

#define WS_XSYSLINK	WS_VISIBLE | WS_CHILD | WS_TABSTOP | LWS_TRANSPARENT | LWS_USEVISUALSTYLE

class CXSysLink :
	public CXTextSizedControl
{
public:
	CXSysLink();
	~CXSysLink();
	void SetLink(string szLink, bool bUpdate = true);
	HRESULT OnNotify(WPARAM wParam, LPARAM lParam);
private:
	string szLink;
};

#define Syslink_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ESYSLINK)