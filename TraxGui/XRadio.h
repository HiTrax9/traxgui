#pragma once
#include "XStateControl.h"
#define WS_XRADIO WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_OWNERDRAW
class CXRadio :
	public CXStateControl
{
public:
	CXRadio();
	~CXRadio();
private:
	//void DetermineTypeAndState();
	//void AdjustRects(HDC hdc);
};

typedef std::shared_ptr<CXRadio> PXRadio;

#define Radio_MakeCP(id, x, y, s) CXControl::MakeCP(id, x, y, 0, 0, s, ERADIO)