#include "XButton.h"

CXButton::CXButton()
{
	SetClass(_T("BUTTON"));
	SetStyle(BS_XBUTTON);
	SetControlType(EBUTTON);
}

CXButton::~CXButton()
{
}

XControlProperties& CXButton::TransformCP(XControlProperties & cp)
{
	cp.controlType = EBUTTON;
	return cp;
}

int CXButton::Create(bool bRegister)
{
	if (CXTextSizedControl::Create(false) != S_OK)
		return S_FALSE;

	//// this is the little hack to show the access key underline on the buttons without having to press alt...
	//// this has a side effect of the window having focus, just not really responding to
	//// messages the same way. will need to figure out.
	keybd_event(VK_MENU, 0x38, 0, 0);
	Redraw();
	keybd_event(VK_MENU, 0x38, KEYEVENTF_KEYUP, 0);

	keybd_event(VK_MENU, 0x38, 0, 0);				// this a real ghetto fix to the bug above
	keybd_event(VK_MENU, 0x38, KEYEVENTF_KEYUP, 0);	// this a real ghetto fix to the bug above
	return S_OK;
}

void CXButton::Disable(bool b)
{
	CXControl::Disable(b);
	Button_Enable(hWnd, (b) ? false : true);
}

void CXButton::SetIcon(HANDLE hIcon)
{
	SendMessage(hWnd, BM_SETIMAGE, IMAGE_ICON, (LPARAM)hIcon);
}

void CXButton::SetBitmap(HANDLE hBitmap)
{
	SendMessage(hWnd, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBitmap);
}

HRESULT CXButton::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch (HIWORD(wParam))
	{
		case BN_CLICKED:
		{
			CallFunction(CLICK_EID);
			break;
		}
		case BN_DBLCLK:
		{
			CallFunction(CLICK_EID);
			break;
		}
	}
	return 0;
}