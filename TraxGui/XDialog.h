#pragma once
#include "XMessageHandlerWindow.h"

#define WS_XDIALOG WS_VISIBLE | WS_SYSMENU
#define XDIALOGCLASS "XDialogWnd01"

class CXDialog : public CXMessageHandlerWindow
{
	static bool bRegistered;
public:
	CXDialog(HINSTANCE hInstance, WNDPROC WndProc, string szTitle);
	CXDialog(PXWindow& pOwner, string szTitle);
	~CXDialog();
	virtual int Create();
	virtual void Show();
	virtual void Destroy();
	virtual int PostCreateControls();
};

typedef std::shared_ptr<CXDialog> PXDialog;