#pragma once
#include "XControl.h"

typedef uint XFnID;
using FXFunction = int(void*);
typedef std::function<FXFunction> FXControlCallback;

struct FXControlFunction
{
	FXControlCallback pFunc;
	void* pData = nullptr;

	FXControlFunction()
	{
		pFunc = nullptr;
		pData = nullptr;
	}

	FXControlFunction(FXControlCallback pFunc, void* pData)
	{
		this->pFunc = pFunc;
		this->pData = pData;
	}

	FXControlFunction(FXFunction pFunc, void* pData)
	{
		this->pFunc = pFunc;
		this->pData = pData;
	}

	void SetData(void* pData)
	{
		this->pData = pData;
	}

	int Call()
	{
		return pFunc(pData);
	}

	int Call(void* pData)
	{
		return pFunc(pData);
	}
};

typedef std::map<XFnID, FXControlFunction> FnMap;

typedef int EVENT_ID;
enum EEVENT_ID : EVENT_ID
{
	NOTIFY_EID,
	CLICK_EID,
	DBLCLICK_EID,
	MOUSEMOVE_EID,
	MOUSELEAVE_EID,
	SETCURSOR_EID,
	EVENTID_LAST // should always be last.
};

class CXFunctionalControl : public CXControl
{
public:
	CXFunctionalControl();
	~CXFunctionalControl();

protected:
	void SetFunction(XFnID xFnID, FXFunction pFunc, void* pData = nullptr);
	void SetFunction(XFnID xFnID, FXControlCallback pFunc, void* pData = nullptr);
	int CallFunction(XFnID xFnID);
	int CallFunction(XFnID xFnID, void* pData);

public: // event handler setters
	void SetEventHandler(EVENT_ID eID, FXControlCallback pFunc, void* pData = nullptr);
	void SetEventHandler(EVENT_ID eID, FXFunction pFunc, void* pData = nullptr);
	void SetEventHandler(EVENT_ID eID, FXFunction pFunc, int pData = 0);

public: // message handlers
	virtual HRESULT OnNotify(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnSetCursor(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseMove(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnCommand(WPARAM wParam, LPARAM lParam);
private:
	FnMap fnMap;
};
