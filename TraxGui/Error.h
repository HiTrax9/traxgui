#pragma once
#include <Windows.h>
#include "MsgBox.h"
#include "Misc\cstring.h"

namespace Error
{
	cstring GetLastErrorMsg();
	void Box(string szMsg);
	void MsgBoxGLE();
	void MsgBoxSGLE(cstring szMsg);
}
