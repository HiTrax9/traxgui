#include "XGroupbox.h"
#include "XMainWindow.h"
CXGroupbox::CXGroupbox()
{
	// we can still use the theme drawing funtions
	// and avoid annoying button characteristics
	// had we used WC_BUTTON, the control would draw
	SetClass(WC_STATIC);	// over the controls inside of the groupbox and
	SetStyle(WS_XGROUPBOX);	// and ignore user input... haxlyfe
}

CXGroupbox::~CXGroupbox()
{
}

int CXGroupbox::Create(bool bRegister)
{
	CXMouseTrackingControl::Create(bRegister);
	//xCtrlMgr = std::make_shared<CXControlManager>(GetPointer());
	xCtrlMgr = pOwner->_dpc<CXControlMgrWindow>()->GetControlManager();
	pOwner->_dpc<CXMessageHandlerWindow>()->AddMsgHandlerWnd(GetPointer());

	return 0;
}

PXControlManager CXGroupbox::GetControlManager()
{
	return xCtrlMgr;
}

void CXGroupbox::DetermineTypeAndState()
{
	iControlPart = BP_GROUPBOX;
	dwState = GBS_NORMAL;
	bFocused = false;
}

void CXGroupbox::AdjustRects(HDC hdc)
{
	TEXTMETRIC tm;
	GetTextMetrics(hdc, &tm);
	rcContent = rcBG;
	rcContent.top += tm.tmHeight;
	rcBG.top += tm.tmHeight / 2; // line the top line with the center of the text
	SIZE szSize{ 0 };
	GetTextExtentPoint32(hdc, szText.c_str(), (int)szText.length(), &szSize);

	RECT rcItem = GetCRect();
	rcText = rcItem;
	rcText.bottom = rcBG.top + tm.tmHeight;
	rcText.left = rcText.left + 20;
	rcText.right = rcText.left + szSize.cx + 8;
}

void CXGroupbox::PostDraw()
{
	for (auto pCon : pControls)
	{
		pCon->Redraw();
	}
}

PXButton CXGroupbox::AddButton(XControlProperties & cp)
{
	cp.controlType = EBUTTON;
	return AddControl<CXButton>(cp);
}

PXEdit CXGroupbox::AddEdit(XControlProperties & cp)
{
	cp.controlType = EEDIT;
	return AddControl<CXEdit>(cp);
}

PXLabel CXGroupbox::AddLabel(XControlProperties & cp)
{
	cp.controlType = ELABEL;
	return AddControl<CXLabel>(cp);
}

PXListBox CXGroupbox::AddListBox(XControlProperties & cp)
{
	cp.controlType = ELISTBOX;
	return AddControl<CXListBox>(cp);
}

PXListView CXGroupbox::AddListView(XControlProperties & cp)
{
	cp.controlType = ELISTVIEW;
	return AddControl<CXListView>(cp);
}

PXIcon CXGroupbox::AddIcon(XControlProperties & cp)
{
	cp.controlType = EICON;
	return AddControl<CXIcon>(cp);
}

PXBitmap CXGroupbox::AddBitmap(XControlProperties & cp)
{
	cp.controlType = EBITMAP;
	return AddControl<CXBitmap>(cp);
}

PXCheckbox CXGroupbox::AddCheckbox(XControlProperties & cp)
{
	cp.controlType = ECHECKBOX;
	return AddControl<CXCheckbox>(cp);
}

PXRadio CXGroupbox::AddRadio(XControlProperties & cp)
{
	cp.controlType = ERADIO;
	return AddControl<CXRadio>(cp);
}

PXGroupbox CXGroupbox::AddGroupbox(XControlProperties & cp)
{
	cp.controlType = EGROUPBOX;
	return AddControl<CXGroupbox>(cp);
}

PXCombobox CXGroupbox::AddCombobox(XControlProperties & cp)
{
	cp.controlType = ECOMBOBOX;
	return AddControl<CXCombobox>(cp);
}

void CXGroupbox::AddControl(PXControl pControl)
{
	pControl->Anchor(eAnkhDir);
	pControls.push_back(pControl);
}

void CXGroupbox::AdjustToBox(PXControl pControl)
{
	int newX = 0, newY = 0;
	POINT ctrlPos = pControl->GetPos();
	POINT gbPos = GetPos();
	TEXTMETRIC tm = this->GetTextMetric();
	newX = ctrlPos.x + gbPos.x;
	newY = ctrlPos.y + gbPos.y + (tm.tmHeight / 2);
	//pControl->SetPos(ctrlPos.x + gbPos.x, ctrlPos.y + gbPos.y);
	pControl->SetPos(newX, newY);
}

//void CXGroupbox::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI)
//{
//	RECT rcRect = GetCRect();
//	HDC hdc = lpDI->hDC;
//	if (!hBorderPen)
//	{
//		hBorderPen = CreatePen(PS_SOLID, 1, clBorder);
//		SelectPen(hdc, hBorderPen);
//	}
//
//	TEXTMETRIC tmx;
//	GetTextMetrics(hdc, &tmx);
//	SIZE textSize;
//	tstring str = _T(" ") + szText + _T(" ");
//	GetTextExtentPoint32(hdc, str.c_str(), lstrlen(str.c_str()), &textSize);
//	MoveToEx(hdc, rcRect.left, rcRect.top, NULL);
//	LineTo(hdc, rcRect.left + 10, rcRect.top);
//	MoveToEx(hdc, rcRect.right - textSize.cx + 5, rcRect.top, NULL);
//	LineTo(hdc, rcRect.right, rcRect.top);
//	LineTo(hdc, rcRect.right, rcRect.bottom);
//	LineTo(hdc, rcRect.left, rcRect.bottom);
//	LineTo(hdc, rcRect.left, rcRect.top);
//
//	RECT rcText;
//	rcText.top = rcRect.top - (textSize.cy / 2);
//	rcText.left = rcRect.left + 10;
//	rcText.right = rcText.left + textSize.cx;
//	rcText.bottom = rcText.top + textSize.cy;
//
//	//SetBkMode(hdc, OPAQUE);
//	SetTextColor(hdc, colText.GetColor());
//	DrawText(hdc, str.c_str(), str.length(), &rcText, DT_LEFT);
//}