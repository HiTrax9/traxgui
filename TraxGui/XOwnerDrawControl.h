#pragma once
#include "XFunctionalControl.h"

class CXOwnerDrawControl :
	public CXFunctionalControl
{
public:
	CXOwnerDrawControl();
	~CXOwnerDrawControl();

public:
	virtual HRESULT OnCtlColor(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnDrawItem(WPARAM wParam, LPARAM lParam);
	virtual void DetermineTypeAndState() {}
	virtual void AdjustRects(HDC hdc) {}
	virtual void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);
	virtual void PostDraw() {}
protected:
	bool bDrawFocus = false;
	bool bFocused = false;
	int iControlPart = 0;
	DWORD dwState = 0;
	RECT rcBG{ 0 };
	RECT rcText{ 0 };
	RECT rcContent{ 0 };
};
