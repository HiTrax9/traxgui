#pragma once
//#include <tchar.h>
#define _EMsgBox(s) MessageBox(NULL, s, _T("Error!"), MB_ICONERROR | MB_OK)
#define EMsgBox(s, t) MessageBox(NULL, s, t, MB_ICONERROR | MB_OK)
#define _IMsgBox(s) MessageBox(NULL, s, _T("Information"), MB_ICONINFORMATION | MB_OK)
#define IMsgBox(s, t) MessageBox(NULL, s, t, MB_ICONINFORMATION | MB_OK)

#define MsgBox(s, t) MessageBox(NULL, s, t, MB_ICONINFORMATION | MB_OK)