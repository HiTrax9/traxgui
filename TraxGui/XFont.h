#pragma once
#include "stdafx.h"
#include "Error.h"

#define DEFAULT_FONT_SIZE 9

typedef int FontID;
typedef int SizePoint;

class CXFont;
typedef std::shared_ptr<CXFont> PXFont;

class CXFont : public std::enable_shared_from_this<CXFont>
{
public:
	CXFont();
	CXFont(string szFace, int height, int width = 0);
	~CXFont();

	HFONT Create();
	// need to call create again after this.
	void Reset(string szFace, int height, int width = 0);
	HFONT Get();
	SizePoint Size();
	void UsePoints(bool bBool);
	PXFont GetPointer();
	FontID GetID();

	void SetID(FontID id);
	void Release();
private:
	FontID id;
	string szFace;
	HFONT hFont = nullptr;
	bool bPoints = true;
	int iWidth = 0;
	int iHeight = 0;
	int iWeight = 0;
	bool bItalic = false;
	bool bStriked = false;
	bool bUnderlined = false;
	DWORD dwCharset = ANSI_CHARSET;
};

class CXFontManager
{
	static std::map<FontID, PXFont> mFonts;
public:
	// non-creation ctor
	static PXFont GetFont(FontID fontID);
	//creation ctor
	static PXFont GetFont(FontID fontID, string szFace);
	static PXFont GetFont(FontID fontID, string szFace, SizePoint uSize);
	static HFONT AddFont(FontID fontID, string szFace, SizePoint uSize);
	static HFONT ReplaceFont(FontID fontID, string szFace, SizePoint uSize);
};