#pragma once
#include <Windows.h>
#include <windowsx.h>
#include <Uxtheme.h>
#include <vsstyle.h>
#include <map>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <assert.h>

#pragma comment(lib, "UxTheme.lib")
#pragma comment(lib, "Comctl32.lib")
//applies current windows theme to controls.
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include "Misc\cstring.h"

typedef UINT uint;
#define recast reinterpret_cast

#pragma warning(disable : 4302)