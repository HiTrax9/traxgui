#include "cstring.h"

//couple helpers
std::wstring MBS2WCS(std::string szString)
{
	size_t cc;
	std::wstring szBuf(szString.length() * 2, 0);
	mbstowcs_s(&cc, &szBuf[0], szBuf.size(), szString.c_str(), szString.length());
	return szBuf.c_str();
}

std::string WCS2MBS(std::wstring szString)
{
	size_t cc;
	std::string szBuf(szString.length() + 1, 0);
	wcstombs_s(&cc, &szBuf[0], szBuf.size(), szString.c_str(), szString.length());
	return szBuf.c_str();
}

//class implementation
cstring::cstring()
{
	s = _T("");
	bufferA = "";
	bufferW = L"";
}

cstring::cstring(tstring s)
{
	this->s = s;
}

cstring::cstring(const char * s)
{
	bufferA = s;
	this->s = MBS2WCS(s);
}

cstring::cstring(const wchar_t * s)
{
	this->s = s;
}

cstring::cstring(const int & iNum)
{
	s.resize(sizeof(tchar) * 16, 0);
	_itot_s(iNum, &this->s[0], s.size(), 10);
}

cstring::~cstring()
{
	if (this)
	{
		if (!s.empty())
			s.clear();

		if (!bufferA.empty())
			bufferA.clear();

		if (!bufferW.empty())
			bufferW.clear();
	}
}

const wchar_t * cstring::c_strW()
{
#if _UNICODE
	return s.c_str();
#else
	return MBS2WCS(s).c_str();
#endif
}

const char * cstring::c_strA()
{
#if _UNICODE
	this->bufferA = WCS2MBS(s).c_str();
	return bufferA.c_str();
#else
	return s.c_str();
#endif
}

#if _UNICODE
const wchar_t * cstring::c_str()
{
	return c_strW();
}
#else
const char * cstring::c_str()
{
	return c_strA();
}
#endif