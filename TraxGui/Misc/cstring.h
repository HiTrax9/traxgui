#pragma once
#include <string>
#include <string>
#include <tchar.h>

#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif

typedef tstring string;

typedef TCHAR tchar;

std::wstring MBS2WCS(std::string szString);
std::string WCS2MBS(std::wstring szString);

class cstring
{
public:
	cstring();
	cstring(const tstring s);
	cstring(const char * s);
	cstring(const wchar_t * s);
	cstring(const int& iNum);
	~cstring();
	const wchar_t* c_strW();
	const char* c_strA();

#if _UNICODE
	const wchar_t* c_str();
#else
	const char* c_str();
#endif

	cstring& operator=(const tstring& rhs)
	{
		s = rhs;
		return *this;
	}

	cstring& operator=(const wchar_t* rhs)
	{
		s = rhs;
		return *this;
	}

	cstring& operator=(const char* rhs)
	{
		s = MBS2WCS(rhs);
		return *this;
	}

	cstring& operator+=(const cstring& rhs)
	{
		s += rhs.s;
		return *this;
	}

	tchar& operator[](const size_t index)
	{
		return s[index];
	}

	bool operator==(const cstring& rhs)
	{
		return (s == rhs.s);
	}

	bool operator!=(const cstring& rhs)
	{
		return (s != rhs.s);
	}

public:
	tstring s = _T("");
private:
	std::string bufferA = "";
	std::wstring bufferW = L"";
};
