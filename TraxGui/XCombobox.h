#pragma once
#include "XTextSizedControl.h"
#include "XCollectionControl.h"
#define WS_XCOMBOBOX WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWNLIST | CBS_HASSTRINGS

class CXCombobox :
	public CXTextSizedControl, public CXCollectionControl
{
public:
	CXCombobox();
	~CXCombobox();
	int Create(bool bRegister = false);
	void AddItem(CXItem xItem);
	void AddItem(string szText);
	void SetSelectedItem(int iItem = -1);
};

typedef std::shared_ptr<CXCombobox> PXCombobox;

#define Combobox_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ECOMBOBOX)