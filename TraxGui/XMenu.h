#pragma once
#include "XGuiBase.h"

class CXMenu;
typedef std::shared_ptr<CXMenu> PXMenu;

struct XMenuItem;
typedef std::shared_ptr<XMenuItem> PXMenuItem;
using FXMenuItemCallback = int(void*);

struct XMenuItem : std::enable_shared_from_this<XMenuItem>
{
	static PXMenuItem Make(XID id, string szText, FXMenuItemCallback* pFn = nullptr)
	{
		return std::make_shared<XMenuItem>(id, szText, pFn);
	}

	static PXMenuItem Make(XID id, string szText, bool bHasSubMenu, FXMenuItemCallback* pFn = nullptr)
	{
		PXMenuItem pItem = std::make_shared<XMenuItem>(id, szText, pFn);
		pItem->AddSubMenu();
		return pItem;
	}

	XMenuItem(XID id, string szText, FXMenuItemCallback* pFn = nullptr)
	{
		this->id = id;
		this->szText = szText;
		this->pFn = pFn;
	}

	void SetOwnerMenu(PXMenu pOwner)
	{
		this->pOwner = pOwner;
	}

	PXMenu AddSubMenu();
	PXMenu GetOwnerMenu() { return pOwner; }
	PXMenu GetSubMenu() { return pSubMenu; }

	XID id = 0;
	string szText;
	FXMenuItemCallback* pFn = nullptr;
	bool bHasSubMenu = false;

private:
	PXMenu pOwner = nullptr;
	PXMenu pSubMenu = nullptr;
};

typedef std::map<XID, PXMenuItem> XMenuMap;
typedef std::vector<PXMenuItem> VXMenuItems;

class CXMenu : public std::enable_shared_from_this<CXMenu>
{
public:
	static PXMenu Make(bool bContextMenu = false);
	CXMenu(bool bPopup = false);
	~CXMenu();
	PXMenuItem AddItem(PXMenuItem pItem, PXMenu pOwner = nullptr);
	XMenuMap& GetItems();
	HMENU Get();

private:
	VXMenuItems vItems;
	XMenuMap mMenuItems;
	HMENU hMenu = NULL;

public:
	PXMenu pOwnerMenu = nullptr;
	bool bIsSubMenu = false;
	bool bIsPopupMenu = false;
};
