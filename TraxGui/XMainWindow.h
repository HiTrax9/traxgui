#pragma once
#include "XMessageHandlerWindow.h"

#define MAINWNDCLASS "MainWndClass00"
#define MAINWNDSTYLE0 WS_VISIBLE | WS_OVERLAPPEDWINDOW
#define MAINWNDSTYLE1 WS_VISIBLE | WS_SYSMENU

extern LRESULT CALLBACK MainWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

class CXMainWindow;
typedef std::shared_ptr<CXMainWindow> PXMainWindow;

class CXMainWindow : public CXMessageHandlerWindow
{
	static PXMainWindow pMainWindow;
public:
	static PXMainWindow Get();
	CXMainWindow(HINSTANCE hInstance, string szTitle);
	CXMainWindow(HINSTANCE hInstance, string szTitle, uint w, uint h);
	~CXMainWindow();
	void Initialize(uint w, uint h);
	int Create(bool bRegister = false);
	void Destroy();
};
