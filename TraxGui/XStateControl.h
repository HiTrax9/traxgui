#pragma once
#include "XMouseTrackingControl.h"

class CXStateControl :
	public CXMouseTrackingControl
{
public:
	CXStateControl();
	~CXStateControl();
	void SetDimensions(uint w, uint h, bool bUpdate = true);
	//virtual void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);
	virtual int Create(bool bRegister = false);
	virtual HRESULT OnSetFocus(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnKillFocus(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnCommand(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseMove(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);

private:
	void DetermineTypeAndState();
	void AdjustRects(HDC hdc);
protected:
	bool bChecked = false;
	bool bHot = false;
};
