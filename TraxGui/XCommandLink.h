#pragma once
#include "XButton.h"

#define WS_XCOMMANDLINK BS_XBUTTON | BS_ICON | BS_COMMANDLINK

class CXCommandLink :
	public CXButton
{
public:
	CXCommandLink();
	~CXCommandLink();
	void SetNote(string szText);
};

#define Commandlink_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ECOMMANDLINK)