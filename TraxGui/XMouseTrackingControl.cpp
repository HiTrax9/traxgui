#include "XMouseTrackingControl.h"

LRESULT CALLBACK MouseTrackingCallback(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
{
	switch (uMsg)
	{
		case WM_SETCURSOR:
		{
			if (dwRefData)
			{
				if (recast<CXMouseTrackingControl*>(dwRefData)->OnSetCursor(wParam, lParam) != S_OK)
					return S_FALSE;
				return TRUE;
			}
		}
		case WM_MOUSEMOVE:
		{
			if (dwRefData)
			{
				recast<CXMouseTrackingControl*>(dwRefData)->OnMouseMove(wParam, lParam); // oh...
			}
			break;
		}
		case WM_MOUSELEAVE:
		{
			if (dwRefData)
			{
				recast<CXMouseTrackingControl*>(dwRefData)->OnMouseLeave(wParam, lParam);
			}
			break;
		}
	}
	return DefSubclassProc(hWnd, uMsg, wParam, lParam);
}

CXMouseTrackingControl::CXMouseTrackingControl()
{
}

CXMouseTrackingControl::~CXMouseTrackingControl()
{
}

int CXMouseTrackingControl::Create(bool bRegister)
{
	if (CXOwnerDrawControl::Create(bRegister) != S_OK)
		return S_FALSE;
	if (SetWindowSubclass(hWnd, MouseTrackingCallback, 1, (DWORD_PTR)this) == FALSE)
		return S_FALSE;
	return S_OK;
}

void CXMouseTrackingControl::EnableMouseTracking()
{
	TRACKMOUSEEVENT tme{ 0 };
	tme.cbSize = sizeof(tme);
	tme.dwFlags = TME_HOVER | TME_LEAVE;
	tme.dwHoverTime = 0;
	tme.hwndTrack = hWnd;
	if (_TrackMouseEvent(&tme) == FALSE)
	{
		// lol, this is like the first time i do this...
		Error::MsgBoxSGLE(_T("Failed to enable mouse tracking on label..."));
	}
}

HRESULT CXMouseTrackingControl::OnSetCursor(WPARAM wParam, LPARAM lParam)
{
	if (hCursor)
		hCursorOld = ::SetCursor(hCursor);
	return S_OK;
}

bool bSet = false;
HRESULT CXMouseTrackingControl::OnMouseMove(WPARAM wParam, LPARAM lParam)
{
	if (bTracking)
		EnableMouseTracking();
	//if (hCursor && !bSet)
	//{
	//	if (!hCursorOld)
	//		hCursorOld = hCursor;
	//	SetCursor(hCursor);
	//	bSet = true;
	//}

	return CXFunctionalControl::OnMouseMove(wParam, lParam);
}

HRESULT CXMouseTrackingControl::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	if (hCursorOld)
	{
		bSet = false;
		SetCursor(hCursorOld);
		hCursorOld = 0; // in case the cursor gets changed... preemptive thinking
	}
	return CXFunctionalControl::OnMouseLeave(wParam, lParam);
}