#pragma once
#include "XCollectionControl.h"

#define WS_XLISTBOX WS_VISIBLE | WS_TABSTOP | WS_CHILD | WS_BORDER | WS_VSCROLL	| LBS_HASSTRINGS | LBS_NOTIFY

enum ELABELEVENT_ID : EVENT_ID
{
	SETFOCUS_EID = EVENTID_LAST,
	KILLFOCUS_EID
};

class CXListBox :
	public CXCollectionControl, public CXFunctionalControl
{
public:
	//static XControlProperties&
	CXListBox();
	~CXListBox();

	int Create(bool bRegister = false);
	void AddItem(string szText, void* pItemData = nullptr);
	void RemoveItem(uint uIndex);
	int GetSelectedIndex();
	string GetSelectedText();
	bool ItemIsSelected(int index);
	uint GetItemCount();
	void ClearList();
public:
	HRESULT OnCommand(WPARAM wParam, LPARAM lParam);
};

typedef std::shared_ptr<CXListBox> PXListBox;

#define ListBox_MakeCP(id, x, y, w, h) CXControl::MakeCP(id, x, y, w, h, _T(""), ELISTBOX)
#define ListBox_MakeCP_(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ELISTBOX)