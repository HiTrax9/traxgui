#include "XStateControl.h"

CXStateControl::CXStateControl()
{
}

CXStateControl::~CXStateControl()
{
}

void CXStateControl::SetDimensions(uint w, uint h, bool bUpdate)
{
	CXTextSizedControl::SetDimensions(w, h, bUpdate);
	CXTextSizedControl::SetDimensions(GetWidth()+7, GetHeight());
}

int CXStateControl::Create(bool bRegister)
{
	if (CXMouseTrackingControl::Create(bRegister))
		return S_FALSE;
	EnableMouseTracking();
	return S_OK;
}

HRESULT CXStateControl::OnSetFocus(WPARAM wParam, LPARAM lParam)
{
	this->bFocused = false;
	this->bDrawFocus = true;
	Redraw();
	return 0;
}

HRESULT CXStateControl::OnKillFocus(WPARAM wParam, LPARAM lParam)
{
	this->bDrawFocus = true;
	this->bFocused = true;
	Redraw();
	//DrawFocusRect(GetDC((HWND)wParam), &GetCRect());
	//DrawFocusRect(GetWndDC(), &GetCRect());
	return 0;
}

HRESULT CXStateControl::OnCommand(WPARAM wParam, LPARAM lParam)
{
	bChecked = !bChecked;

	return CXFunctionalControl::OnCommand(wParam, lParam);
}

HRESULT CXStateControl::OnMouseMove(WPARAM wParam, LPARAM lParam)
{
	if (!bHot)
	{
		this->bHot = true;
		Redraw();
	}
	return CXMouseTrackingControl::OnMouseMove(wParam, lParam);
}

HRESULT CXStateControl::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	if (bHot)
	{
		this->bHot = false;
		Redraw();
	}
	return CXMouseTrackingControl::OnMouseLeave(wParam, lParam);
}

void CXStateControl::DetermineTypeAndState()
{
	switch (controlType)
	{
		case ERADIO:
			iControlPart = BP_RADIOBUTTON;
			if (bChecked && !bDisabled)
			{
				dwState = RBS_CHECKEDNORMAL;
			}
			else
			{
				dwState = RBS_UNCHECKEDNORMAL;
			}
			if (bHot && !bDisabled)
				dwState |= RBS_HOT;
			break;

		case ECHECKBOX:
			iControlPart = BP_CHECKBOX;
			if (bChecked)
			{
				dwState = CBS_CHECKEDNORMAL;
			}
			else
			{
				dwState = CBS_UNCHECKEDNORMAL;
			}

			if (bHot && !bDisabled)
				dwState |= CBS_HOT;
			break;

			//going to move this
			//case EGROUPBOX:
			//	iControlPart = BP_GROUPBOX;
			//	dwState = GBS_NORMAL;
			//	break;
	}
}

void CXStateControl::AdjustRects(HDC hdc)
{
	int cb_size = 20;
	//rcBG.right = rcBG.left + 20; //this puts the checkbox/radio to the left of the control
	//rcText.left = rcText.left + 20; // this puts the text to the right of the checkbox/radio
	rcBG.right = rcBG.left + cb_size; //
	rcText.left = rcText.left + cb_size;
}

//void CXStateControl::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI)
//{
//	RECT rcBG, rcText;
//	HFONT font = (HFONT)SendMessage(hWnd, WM_GETFONT, 0, 0);
//
//	int cb_size = 13;
//	GetClientRect(hWnd, &rcBG);
//
//	DetermineTypeAndState();
//
//	TEXTMETRIC tm;
//	GetTextMetrics(lpDI->hDC, &tm);
//
//	GetThemeBackgroundContentRect(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &rcText);
//	//if (controlType == EGROUPBOX)
//	//{
//	//	rcBG.top += tm.tmHeight / 2; // line the top line with the center of the text
//	//	SIZE szSize{ 0 };
//	//	GetTextExtentPoint32(lpDI->hDC, szText.c_str(), szText.length(), &szSize);
//
//	//	RECT rcItem = lpDI->rcItem;
//	//	rcText = rcItem;
//	//	rcText.bottom = rcBG.top + tm.tmHeight;
//	//	rcText.left = rcText.left + cb_size;
//	//	rcText.right = rcText.left + szSize.cx + 8;
//	//}
//	//else
//	{
//		rcBG.right = rcBG.left + 20;
//		//rcText.top = rcBG.top + (tm.tmHeight / 4);
//		rcText.left = rcText.left + 20;
//	}
//
//	DTBGOPTS dtbgOpts{ 0 };
//	dtbgOpts.dwSize = sizeof(dtbgOpts);
//	dtbgOpts.dwFlags |= DTBG_VALIDBITS;
//	dtbgOpts.rcClip = rcBG;
//
//	SelectBrush(lpDI->hDC, colBG.GetBrush());
//	RECT rc = rcBG;
//	rc.left += 1;
//	rc.right -= 1;
//	rc.top += 1;
//	rc.bottom -= 1;
//
//	DrawThemeBackgroundEx(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &dtbgOpts);
//	GetThemeBackgroundContentRect(hTheme, lpDI->hDC, iControlPart, dwState, &rcBG, &GetCRect());
//
//
//	//if (controlType == EGROUPBOX)
//	//{
//	//	//COLORREF clBgr = pOwner->GetBGColor().GetBrush();
//	//	//hBgr = CreateSolidBrush(clBgr);
//	//	colBG = pOwner->GetBGColor().GetBrush();
//	//	RECT rcLine = rcText;
//	//	rcLine.bottom -= 5;
//	//	rcLine.top += 5;
//	//	FillRect(lpDI->hDC, &rcLine, colBG.GetBrush());
//	//	rcText.left += 4;
//	//}
//
//	DTTOPTS dtOpts{ 0 };
//	dtOpts.dwSize = sizeof(dtOpts);
//	COLORREF clDisabled = 0;
//	if (bDisabled)
//	{
//		clDisabled = colText.GetColor();
//		for (int x = 0; x < sizeof(COLORREF); x++)
//		{
//			unsigned char* pChar = ((unsigned char*)&clDisabled);
//			unsigned char c = pChar[x];
//			pChar[x] -= c / 4;
//			((unsigned char*)&clDisabled)[x] = pChar[x];
//		}
//
//	}
//	dtOpts.crText = bDisabled ? clDisabled : colText.GetColor();
//	dtOpts.dwFlags |= DTT_TEXTCOLOR;
//
//	DrawThemeTextEx(hTheme, lpDI->hDC, iControlPart, dwState, szText.c_str(), szText.length(), DT_NOCLIP, &rcText, &dtOpts);
//}