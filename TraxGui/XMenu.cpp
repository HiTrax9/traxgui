#include "XMenu.h"

PXMenu CXMenu::Make(bool bContextMenu)
{
	return std::make_shared<CXMenu>(bContextMenu);
}

CXMenu::CXMenu(bool bPopup)
{
	if (bPopup)
	{
		hMenu = CreatePopupMenu();
		this->bIsPopupMenu = true;
	}
	else
	{
		hMenu = CreateMenu();
		this->bIsPopupMenu = false;
	}
}

CXMenu::~CXMenu()
{
}

PXMenuItem CXMenu::AddItem(PXMenuItem pItem, PXMenu pOwner)
{
	MENUITEMINFO mi{ 0 };
	mi.cbSize = sizeof(MENUITEMINFO);
	mi.fMask = MIIM_STRING | MIIM_ID;
	mi.wID = (UINT)pItem->id;
	mi.cch = (UINT)pItem->szText.length();
	mi.dwTypeData = &pItem->szText[0];

	if (pItem->bHasSubMenu)
	{
		mi.fMask |= MIIM_SUBMENU;
		mi.hSubMenu = pItem->GetSubMenu()->hMenu;
	}

	InsertMenuItem(hMenu, (UINT)pItem->id, MF_BYCOMMAND, &mi);

	if (pOwner)
		pItem->SetOwnerMenu(pOwner);
	else if (this->pOwnerMenu)
		pItem->SetOwnerMenu(pOwnerMenu);
	else
		pItem->SetOwnerMenu(shared_from_this());

	if (!bIsSubMenu)
	{
		mMenuItems[pItem->id] = pItem;
	}
	else
	{
		pItem->GetOwnerMenu()->GetItems()[pItem->id] = pItem;
	}
	return pItem;
}

XMenuMap& CXMenu::GetItems()
{
	return mMenuItems;
}

HMENU CXMenu::Get()
{
	return hMenu;
}

PXMenu XMenuItem::AddSubMenu()
{
	if (!bHasSubMenu)
	{
		//CXMenu menu;
		bHasSubMenu = true;
		this->pSubMenu = std::make_shared<CXMenu>(true);
		//this->pSubMenu = menu.shared_from_this();
		pSubMenu->bIsSubMenu = true;
		pSubMenu->pOwnerMenu = this->pOwner;
	}
	return pSubMenu;
}