#pragma once
//#define XCM
#include "XButton.h"
#include "XEdit.h"
#include "XLabel.h"
#include "XListBox.h"
#include "XListView.h"
#include "XImage.h"
#include "XCheckbox.h"
#include "XRadio.h"
#include "XGroupbox.h"
#include "XTrackbar.h"
#include "XCombobox.h"
#include "XCommandLink.h"
#include "XSysLink.h"

class CXGroupbox; //this fucker is a bit of a special case...
typedef std::shared_ptr<CXGroupbox> PXGroupbox;

typedef std::map<XID, XID> XControlManagerMap;
typedef XControlManagerMap XCMMap;

class CXControlManager
{
public:
	CXControlManager(PXWindow pOwner);
	~CXControlManager();

	PXButton CreateButton(XControlProperties& cp);
	PXEdit CreateEdit(XControlProperties& cp);
	PXLabel CreateLabel(XControlProperties& cp);
	PXListBox CreateListBox(XControlProperties& cp);
	PXListView CreateListView(XControlProperties& cp);
	PXIcon CreateIcon(XControlProperties& cp);
	PXBitmap CreateBitmap(XControlProperties& cp);
	PXCheckbox CreateCheckbox(XControlProperties& cp);
	PXRadio CreateRadio(XControlProperties& cp);
	PXGroupbox CreateGroupbox(XControlProperties& cp);
	PXCombobox CreateCombobox(XControlProperties& cp);

	XControlMap& GetControls();

	template <class T>
	std::shared_ptr<T> GetControl(XID id);

	template <class T>
	std::shared_ptr<T> CreateControl(XControlProperties& cp);

private:
	template<class T>
	std::shared_ptr<T> CreateThenCast(XControlProperties& cp);

public:
	PXControl operator[] (XID id)
	{
		return mControls[id];
	}

private:
	PXWindow pOwner;
	XControlMap mControls;
	XCMMap xmControls;
};

typedef std::shared_ptr<CXControlManager> PXControlManager;

template<class T>
inline std::shared_ptr<T> CXControlManager::GetControl(XID id)
{
	//if (mControls[id])
	//	return mControls[id]->_dpc<T>();
	//return std::shared_ptr<T>(nullptr);

	if (CXControl::Get(xmControls[id]))
		return CXControl::Get(xmControls[id])->_dpc<T>();
	else if (mControls[id])
		return mControls[id]->_dpc<T>();
	return nullptr;
}

template<class T>
inline std::shared_ptr<T> CXControlManager::CreateControl(XControlProperties& cp)
{
	if (cp.xUserID)
	{
		mControls[cp.xUserID] = CXControl::Make<T>(pOwner, cp);
	}
	else
	{
		PXControl pControl = CXControl::Make<T>(pOwner, cp);
		if (mControls[pControl->GetID()])
		{
			Error::Box(_T("One of the internal control ID's are colliding with a user id.\n"));
			exit(1);
		}
		mControls[pControl->GetID()] = pControl;
	}
	return CreateThenCast<T>(cp);
}

template<class T>
inline std::shared_ptr<T> CXControlManager::CreateThenCast(XControlProperties& cp)
{
	if (mControls[cp.xUserID]->Create() != S_OK)
		return nullptr;
	xmControls[cp.xUserID] = mControls[cp.xUserID]->GetID();
	return mControls[cp.xUserID]->_dpc<T>();
}

class CXControlMgrWindow : public CXWindow
{
	PXControlManager pControlMgr;
	virtual PXControlManager CreateControlManager();
public:
	CXControlMgrWindow() = default;
	CXControlMgrWindow(HINSTANCE hInstance, WNDPROC WndProc);
	~CXControlMgrWindow() = default;

	virtual PXControlManager GetControlManager();
	virtual int Create(bool bRegisterClass = false);
	// override me and populate with control creation for 'this' window
	virtual int CreateControls() { return 0; }
	virtual int PostCreateControls() { return 0; }
	virtual int ReverseTabOrder();
};