#include "XControlManager.h"
#include "XMessageHandlerWindow.h"

CXControlManager::CXControlManager(PXWindow pOwner)
	: pOwner(pOwner)
{
}

CXControlManager::~CXControlManager()
{
}

PXButton CXControlManager::CreateButton(XControlProperties& cp)
{
	cp.controlType = EBUTTON;
	//if(cp.xUserID)
	//	mControls[cp.xUserID] = CXControl::Make<CXButton>(pOwner, cp);
	//else
	//{
	//	PXButton pBtn = CXControl::Make<CXButton>(pOwner, cp);
	//	mControls[pBtn->GetID()] = pBtn->GetPointer()->_dpc<CXControl>();
	//}
	return CreateControl<CXButton>(cp);
}

PXEdit CXControlManager::CreateEdit(XControlProperties& cp)
{
	cp.controlType = EEDIT;
	//mControls[cp.xUserID] = CXControl::Make<CXEdit>(pOwner, cp);
	return CreateControl<CXEdit>(cp);
}

PXLabel CXControlManager::CreateLabel(XControlProperties& cp)
{
	cp.controlType = ELABEL;
	//mControls[cp.xUserID] = CXControl::Make<CXLabel>(pOwner, cp);
	return CreateControl<CXLabel>(cp);
}

PXListBox CXControlManager::CreateListBox(XControlProperties & cp)
{
	cp.controlType = ELISTBOX;
	//mControls[cp.xUserID] = CXControl::Make<CXListBox>(pOwner, cp);
	return CreateControl<CXListBox>(cp);
}

PXListView CXControlManager::CreateListView(XControlProperties & cp)
{
	cp.controlType = ELISTVIEW;
	//mControls[cp.xUserID] = CXControl::Make<CXListView>(pOwner, cp);
	return CreateControl<CXListView>(cp);
}

PXIcon CXControlManager::CreateIcon(XControlProperties & cp)
{
	cp.controlType = EICON;
	//mControls[cp.xUserID] = CXControl::Make<CXIcon>(pOwner, cp);
	return CreateControl<CXIcon>(cp);
}

PXBitmap CXControlManager::CreateBitmap(XControlProperties & cp)
{
	cp.controlType = EBITMAP;
	//[cp.xUserID] = CXControl::Make<CXBitmap>(pOwner, cp);
	return CreateControl<CXBitmap>(cp);
}

PXCheckbox CXControlManager::CreateCheckbox(XControlProperties & cp)
{
	cp.controlType = ECHECKBOX;
	//mControls[cp.xUserID] = CXControl::Make<CXCheckbox>(pOwner, cp);
	return CreateControl<CXCheckbox>(cp);
}

PXRadio CXControlManager::CreateRadio(XControlProperties & cp)
{
	cp.controlType = ERADIO;
	//mControls[cp.xUserID] = CXControl::Make<CXRadio>(pOwner, cp);
	return CreateControl<CXRadio>(cp);
}

PXGroupbox CXControlManager::CreateGroupbox(XControlProperties & cp)
{
	cp.controlType = EGROUPBOX;
	//mControls[cp.xUserID] = CXControl::Make<CXGroupbox>(pOwner, cp);
	return CreateControl<CXGroupbox>(cp);
}

PXCombobox CXControlManager::CreateCombobox(XControlProperties & cp)
{
	cp.controlType = ECOMBOBOX;
	//mControls[cp.xUserID] = CXControl::Make<CXCombobox>(pOwner, cp);
	return CreateControl<CXCombobox>(cp);
}

XControlMap & CXControlManager::GetControls()
{
	return mControls;
}

PXControlManager CXControlMgrWindow::GetControlManager()
{
	if (!pControlMgr)
		return CreateControlManager();
	return pControlMgr;
}

int CXControlMgrWindow::Create(bool bRegisterClass)
{
	if (CXWindow::Create(bRegisterClass) != S_OK)
		return S_FALSE;

	if (CreateControls() != S_OK)
		return S_FALSE;

	if (PostCreateControls() != S_OK)
		return S_FALSE;

	CXColor::Purge();
	Redraw(); // fuh good measure
	return S_OK;
}

int CXControlMgrWindow::ReverseTabOrder()
{
	auto cm = GetControlManager();
	HWND hLast = GetHandle();
	UINT uFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW;
	for (auto p : cm->GetControls())
	{
		SetWindowPos(p.second->GetHandle(), hLast, 0, 0, 0, 0, uFlags);
		hLast = p.second->GetHandle();
	}
	return S_OK;
}

CXControlMgrWindow::CXControlMgrWindow(HINSTANCE hInstance, WNDPROC WndProc)
	: CXWindow(hInstance, WndProc)
{
}

std::shared_ptr<CXControlManager> CXControlMgrWindow::CreateControlManager()
{
	pControlMgr = std::make_shared<CXControlManager>(GetPointer());
	return pControlMgr;
}