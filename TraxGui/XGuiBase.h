#pragma once
#include "stdafx.h"
#include "Error.h"
#include "XColor.h"
#include "XFont.h"

typedef uintptr_t XID;
typedef unsigned int uint;

class CXGuiBase
{
public:
	static int Initialize();

	static bool bInitialized;
};
