#include "XImage.h"

CXImage::CXImage()
{
	SetClass(_T("STATIC"));
	SetStyle(WS_XIMAGE);
}

CXImage::~CXImage()
{
}

PXImage CXImage::Set(HANDLE hImage)
{
	if (this->hImage)
		DeleteObject(hImage);

	this->hImage = hImage;

	if (!uWidth && !uHeight)
	{
		BITMAP bmp{ 0 };
		if (GetControlType() == EICON)
		{
			ICONINFO ii{ 0 };
			if (!GetIconInfo((HICON)hImage, &ii))
			{
				Error::Box(L"Failed to get icon info...");
				return nullptr;
			}
			if (!GetObject(ii.hbmColor, sizeof(BITMAP), &bmp))
			{
				DeleteObject(ii.hbmColor);
				Error::Box(L"Failed to get icon bitmap...");
				return nullptr;
			}
		}
		else if (GetControlType() == EBITMAP)
		{
			if (!GetObject(hImage, sizeof(BITMAP), &bmp))
			{
				Error::Box(L"Failed to get bitmap object...");
				return nullptr;
			}
		}
		SetDimensions(bmp.bmWidth, bmp.bmHeight);
	}
	Redraw();
	return GetPointer()->_dpc<CXImage>();
}

HANDLE CXImage::Load(string szFilepath)
{
	auto ct = GetControlType();
	DWORD dwImageType = 0;

	dwLoadFlags = LR_LOADFROMFILE;

	if (!uWidth && !uHeight)
		dwLoadFlags |= LR_DEFAULTSIZE;

	if (ct == EICON)
	{
		dwImageType = IMAGE_ICON;
	}
	else if (ct == EBITMAP)
	{
		dwImageType = IMAGE_BITMAP;
	}

	HANDLE hImg = LoadImage(NULL, szFilepath.c_str(), dwImageType, 0, 0, dwLoadFlags);
	if (!hImg)
	{
		Error::MsgBoxSGLE(L"Failed to load image...");
		DeleteObject(hImg);
		return 0;
	}
	Set(hImg);
	return hImg;
}

void CXIcon::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDi)
{
	DrawIcon(lpDi->hDC, 0, 0, (HICON)hImage);
}

void CXBitmap::Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDi)
{
	DRAWITEMSTRUCT* pDi = lpDi;
	HDC hdcDest = pDi->hDC;
	HDC hdcMemDC = NULL;
	HGDIOBJ bmpObj;
	BITMAP bmp;

	hdcMemDC = CreateCompatibleDC(hdcDest);

	bmpObj = SelectObject(hdcMemDC, hImage);
	GetObject(hImage, sizeof(BITMAP), &bmp);

	SetStretchBltMode(hdcMemDC, HALFTONE);
	SetStretchBltMode(hdcDest, HALFTONE);
	RECT pRect = GetCRect();

	StretchBlt(hdcDest,
		pRect.left, pRect.top,
		pRect.right - pRect.left,
		pRect.bottom - pRect.top,
		hdcMemDC, 0, 0,
		bmp.bmWidth, bmp.bmHeight, SRCCOPY);

	//BitBlt(hdcDest, 0, 0, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, hdcMemDC, 0, 0, SRCCOPY);

	DeleteObject(bmpObj);
	DeleteDC(hdcMemDC);
}