#pragma once
#include "XMouseTrackingControl.h"

#define WS_XLABEL WS_VISIBLE | WS_CHILD | SS_OWNERDRAW | SS_NOTIFY

class CXLabel : public CXMouseTrackingControl
{
public:
	CXLabel();
	~CXLabel();
	//int Create(bool bRegister = false);
public:
	static XControlProperties& TransformCP(XControlProperties & cp);

public: // custom setters

	void SetText(string szText);
	void SetText(string szText, PXColor colText);
	void SetTransparentBG(bool bTransparentBG = true);
	void SetFont(FontID fontID);
public: // windows message event handlers
	void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);

private:
	string szOldText;
	bool bTransparentBG = true;
};

typedef std::shared_ptr<CXLabel> PXLabel;

#define Label_MakeCP_(id, x, y, s) CXControl::MakeCP(id, x, y, 0, 0, s, ELABEL)
#define Label_MakeCP(id, x, y, w, h, s) CXControl::MakeCP(id, x, y, w, h, s, ELABEL)
