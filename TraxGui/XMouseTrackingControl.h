#pragma once
#include "XTextSizedControl.h"

class CXMouseTrackingControl : public CXTextSizedControl
{
public:
	CXMouseTrackingControl();
	~CXMouseTrackingControl();
	virtual int Create(bool bRegister = false);
	virtual void EnableMouseTracking();
	virtual HRESULT OnSetCursor(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseMove(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);

private:
	bool bTracking = true;
};
