#include "XWindow.h"

XWindowMap CXWindow::mWindows;
PXWindow CXWindow::GetXWindow(HWND hWnd)
{
	return mWindows[hWnd];
}

CXWindow::CXWindow()
	: pOwner(nullptr)
{
	colBG = CXColor::Make(GetSysColor(COLOR_BTNFACE));
	colText = CXColor::Make(GetStockBrush(BLACK_BRUSH));
}

CXWindow::CXWindow(HINSTANCE hInstance, WNDPROC WndProc)
	: pOwner(nullptr)
{
	this->hInstance = hInstance;
	this->WndProc = WndProc;
	colBG = CXColor::Make(GetSysColor(COLOR_BTNFACE));
	colText = CXColor::Make(GetStockBrush(BLACK_BRUSH));
}

CXWindow::~CXWindow()
{
}

int CXWindow::Create(bool bRegisterClass)
{
	assert(hInstance);
	assert(WndProc);
	assert(!szClass.empty()); // there needs to be a class set
	assert(dwStyle); //no style...
	ATOM atom = 0;

	if (bRegisterClass)
	{
		auto wcx = CreateWindowClass(szClass);
		atom = RegisterClassEx(&wcx);
		if (!atom)
		{
			cstring s = L"Failed to register class: ";
			s += szClass;
			Error::MsgBoxSGLE(s);
			return S_FALSE;
		}
	}

	hWnd = CreateWindowEx(
		dwStyleEx,
		(bRegisterClass) ? MAKEINTATOM(atom) : szClass.c_str(),
		szText.c_str(),
		dwStyle,
		iX,
		iY,
		uWidth,
		uHeight,
		(pOwner) ? pOwner->GetHandle() : 0,
		(HMENU)xID,
		hInstance,
		NULL);

	if (!hWnd)
	{
		Error::MsgBoxGLE();
		return S_FALSE;
	}

	if (bHasFont)
		SetFont(fontID);
	else if (pOwner && pOwner->bHasFont)
	{
		SetFont(pOwner->GetFont()->GetID());
		SetTextColor(pOwner->GetFontColor());
	}

	mWindows[hWnd] = GetPointer();

	if (!UpdateWindow(hWnd))
	{
		Error::MsgBoxGLE();
		return S_FALSE;
	}

	if (!ShowWindow(hWnd, (bShow) ? SW_SHOW : SW_HIDE))
	{
		Error::MsgBoxGLE();
		return S_FALSE;
	}

	return 0;
}

WNDCLASSEX CXWindow::CreateWindowClass(string& szClass)
{
	WNDCLASSEX wcx{ 0 };
	wcx.cbSize = sizeof(WNDCLASSEX);
	wcx.style = CS_HREDRAW | CS_VREDRAW;
	wcx.lpfnWndProc = WndProc;
	wcx.cbClsExtra = 0;
	wcx.cbWndExtra = 0;
	wcx.hInstance = hInstance;
	wcx.hCursor = (hCursor) ? hCursor : LoadCursor(NULL, IDC_ARROW);
	wcx.hbrBackground = colBG->GetBrush();
	wcx.lpszMenuName = szMenu.c_str();
	wcx.lpszClassName = szClass.c_str();
	hIcon = wcx.hIcon = (hIcon) ? hIcon : LoadIcon(NULL, IDI_APPLICATION);
	hIconSm = wcx.hIconSm = (hIconSm) ? hIconSm : LoadIcon(NULL, IDI_APPLICATION);
	return wcx;
}

HWND CXWindow::GetHandle()
{
	return hWnd;
}

HINSTANCE CXWindow::GetInstance()
{
	return hInstance;
}

WNDPROC CXWindow::GetWndProc()
{
	return WndProc;
}

RECT CXWindow::GetCRect()
{
	RECT rc{ 0 };
	if (hWnd)
		GetClientRect(hWnd, &rc);
	else
		Error::MsgBoxSGLE("Failed to get rect...");
	return rc;
}

string CXWindow::GetText()
{
	return szText;
}

XID CXWindow::GetID()
{
	return xID;
}

PXFont CXWindow::GetFont()
{
	return CXFontManager::GetFont(fontID);
}

PXColor CXWindow::GetBGColor()
{
	return colBG;
}

PXColor CXWindow::GetFontColor()
{
	return colText;
}

HDC CXWindow::GetWndDC()
{
	return GetDC(hWnd);
}

DWORD CXWindow::GetStyle()
{
	return dwStyle;
}

DWORD CXWindow::GetStyleEx()
{
	return dwStyleEx;
}

uint CXWindow::GetWidth()
{
	return uWidth;
}

uint CXWindow::GetHeight()
{
	return uHeight;
}

POINT CXWindow::GetPos()
{
	POINT p;
	p.x = iX;
	p.y = iY;
	return p;
}

TEXTMETRIC CXWindow::GetTextMetric()
{
	TEXTMETRIC tm{ 0 };
	HDC hdc = GetWndDC();
	SelectObject(hdc, GetFont()->Get());
	GetTextMetrics(hdc, &tm);
	return tm;
}

bool CXWindow::HasMenu()
{
	return bHasMenu;
}

PXWindow CXWindow::GetPointer()
{
	return shared_from_this();
}

void CXWindow::SetInstance(HINSTANCE hInstance)
{
	this->hInstance = hInstance;
}

void CXWindow::SetWndProc(WNDPROC WndProc)
{
	this->WndProc = WndProc;
}

void CXWindow::SetClass(string szClass)
{
	this->szClass = szClass;
}

void CXWindow::SetStyle(DWORD dwStyle)
{
	this->dwStyle = dwStyle;
	if (hWnd)
	{
		SetWindowLongPtr(hWnd, GWL_STYLE, dwStyle);
	}
}

void CXWindow::SetStyleEx(DWORD dwStyleEx)
{
	this->dwStyleEx = dwStyleEx;
}

void CXWindow::SetText(string szText)
{
	this->szText = szText;
	if (hWnd)
	{
		SendMessage(hWnd, WM_SETTEXT, 0, (LPARAM)szText.c_str());
	}
	Redraw();
}

void CXWindow::SetPos(int x, int y, bool bUpdate)
{
	this->iX = x;
	this->iY = y;
	if (bUpdate && hWnd)
	{
		DWORD dwFlags = SWP_SHOWWINDOW | SWP_NOSIZE;
		SetWindowPos(hWnd, NULL, x, y, 0, 0, dwFlags);
		UpdateWindow(hWnd);
	}
}

void CXWindow::SetHeight(uint h, bool bUpdate)
{
	SetDimensions(uWidth, h, bUpdate);
}

void CXWindow::SetWidth(uint w, bool bUpdate)
{
	SetDimensions(w, uHeight, bUpdate);
}

void CXWindow::SetDimensions(uint w, uint h, bool bUpdate)
{
	this->uWidth = w;
	this->uHeight = h;
	if (bUpdate && hWnd)
	{
		DWORD dwFlags = SWP_SHOWWINDOW | SWP_NOMOVE;
		SetWindowPos(hWnd, NULL, iX, iY, uWidth, uHeight, dwFlags);
	}
}

void CXWindow::SetOwner(PXWindow pOwner)
{
	this->pOwner = pOwner;
}

void CXWindow::SetID(XID xID)
{
	this->xID = xID;
}

void CXWindow::SetFont(FontID fontID)
{
	this->fontID = fontID;
	bHasFont = true;
	if (hWnd)
	{
		SendMessage(hWnd, WM_SETFONT, (WPARAM)CXFontManager::GetFont(fontID)->Get(), 0);
		SelectObject(GetWndDC(), CXFontManager::GetFont(fontID)->Get());
	}
	Redraw();
}

void CXWindow::SetBgColor(PXColor xColor)
{
	this->colBG = xColor;
	if (hWnd)
	{
		SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)colBG->GetBrush());
		Redraw();
	}
}

void CXWindow::SetBgColor(COLORREF xColor)
{
	colBG = CXColor::Get(xColor);
	SetBgColor(colBG);
}

void CXWindow::SetTextColor(PXColor xColor)
{
	this->colText = xColor;
	Redraw();
}

void CXWindow::SetTextColor(COLORREF xColor)
{
	colText = CXColor::Get(xColor);
	SetTextColor(colText);
}

void CXWindow::SetCursor(HCURSOR hCursor)
{
	if (this->hCursor)
		DestroyCursor(this->hCursor);

	this->hCursor = hCursor;

	if (hWnd)
		SetClassLongPtr(hWnd, GCLP_HCURSOR, (uintptr_t)hCursor);
}

void CXWindow::SetMenu(PXMenu xMenu)
{
	assert(!xMenu->bIsPopupMenu); //Use SetContextMenu instead...
	bHasMenu = true;
	this->pMenu = xMenu;
	if (!(GetStyle() & WS_CHILD))
		::SetMenu(hWnd, pMenu->Get());
}

void CXWindow::SetContextMenu(PXMenu xMenu)
{
	assert(xMenu->bIsPopupMenu); //Use SetMenu instead...
	pContextMenu = xMenu;
}

void CXWindow::MenuHandler(XID idItem)
{
	if (pMenu)
	{
		auto pItem = pMenu->GetItems()[idItem];
		if (pItem)
		{
			if (pItem->pFn)
				pItem->pFn((void*)idItem);
		}
	}
}

void CXWindow::Clear()
{
	auto dc = GetWndDC();
	RECT rc{ 0 };
	GetClientRect(hWnd, &rc);
	FillRect(dc, &rc, colBG->GetBrush());
	ReleaseDC(hWnd, dc);
}

void CXWindow::Redraw()
{
	if (hWnd)
		RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE | RDW_ERASE);
}

void CXWindow::Destroy()
{
	DestroyWindow(hWnd);
}

HRESULT CXWindow::OnContextMenu(WPARAM wParam, LPARAM lParam)
{
	if (pContextMenu)
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		if (x < 0 && y < 0)
		{
			auto p = GetPos();
			x = p.x;
			y = p.y;
		}
		int id = TrackPopupMenuEx(this->pContextMenu->Get(), TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RETURNCMD, x, y, hWnd, NULL);
		if (id)
			if (pContextMenu->GetItems()[id]->pFn)
				return pContextMenu->GetItems()[id]->pFn((void*)id);
	}
	return 0;
}

HRESULT CXWindow::OnCtlColor(WPARAM wParam, LPARAM lParam)
{
	// shuttin down da warnings...
	// should probably return lresult from this function... idk
	return (HRESULT)(LRESULT)colBG->GetBrush();
}