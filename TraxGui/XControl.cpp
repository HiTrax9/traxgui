#include "XControl.h"
#include "XButton.h"
#include "XEdit.h"
#include "XLabel.h"

XControlMap CXControl::xAppControls;
XControlMap CXControl::xAnchoredControls;

PXControl CXControl::Get(XID id)
{
	return xAppControls[id];
}

XControlProperties CXControl::MakeCP(XID xID, int x, int y, uint w, uint h)
{
	return MakeCP(xID, x, y, w, h, _T(""), EINVALID);
}

XControlProperties CXControl::MakeCP(XID xID, int x, int y, uint w, uint h, string szText)
{
	return MakeCP(xID, x, y, w, h, szText, EINVALID);
}

XControlProperties CXControl::MakeCP(XID xUserID, int x, int y, uint w, uint h, string szText, ECONTROLTYPE controlType)
{
	XControlProperties cp{ 0 };
	cp.xUserID = xUserID;
	cp.x = x;
	cp.y = y;
	cp.w = w;
	cp.h = h;
	cp.szText = szText;
	cp.controlType = controlType;
	return cp;
}

ECONTROLTYPE CXControl::GetControlType()
{
	return controlType;
}

XID CXControl::GetUserID()
{
	return xUserID;
}

void CXControl::Anchor(EANCHORDIR eAnkhDir)
{
	this->eAnkhDir = eAnkhDir;
	xAnchoredControls[GetID()] = GetPointer()->_dpc<CXControl>();
	RECT rcClient = pOwner->GetCRect();
	orgSize = { (LONG)GetWidth(), (LONG)GetHeight() };
	orgPos = { iX, iY };
	anchorDeltaX.cx = rcClient.right - iX;
	anchorDeltaX.cy = rcClient.right - iX - uWidth;
	anchorDeltaY.cx = rcClient.bottom - iY;
	anchorDeltaY.cy = rcClient.bottom - iY - uHeight;
}

void CXControl::Resize()
{
	RECT rcOwner = pOwner->GetCRect();
	//POINT cPos = GetPos(); // this get pos might actually not work like i want...
	int newWidth = 0, newHeight = 0;
	int newX = 0, newY = 0;
	RECT rcControl{ 0 };

	switch (eAnkhDir)
	{
		case INPLACE:
		case LR: //let it fall through to the next case
			rcControl.left = rcOwner.right - anchorDeltaX.cx;
		case RIGHT:
			rcControl.right = rcOwner.right - anchorDeltaX.cy;
			if (eAnkhDir != INPLACE)
				break;
		case TB:
			rcControl.top = rcOwner.bottom - anchorDeltaY.cx;
		case BOTTOM:
			rcControl.bottom = rcOwner.bottom - anchorDeltaY.cy;
			break;
		default:
			return;
	}

	if (!rcControl.left)
		rcControl.left = orgPos.x;

	if (!rcControl.top)
		rcControl.top = orgPos.y;

	newWidth = rcControl.right - rcControl.left;
	if (newWidth)
		SetWidth(newWidth > orgSize.cx ? newWidth : orgSize.cx);

	newHeight = rcControl.bottom - rcControl.top;
	if (newHeight)
		SetHeight(newHeight > orgSize.cy ? newHeight : orgSize.cy);

	SetPos(rcControl.left > orgPos.x ? rcControl.left : orgPos.x,
		rcControl.top > orgPos.y ? rcControl.top : orgPos.y);
}

void CXControl::SetControlType(ECONTROLTYPE controlType)
{
	this->controlType = controlType;
}

void CXControl::SetUserID(XID xUserID)
{
	if (!xUserID)
		this->xUserID = xID;
	else
		xUserID = xID;
}

void CXControl::Disable(bool b)
{
	bDisabled = b;
}