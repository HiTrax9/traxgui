#include "XEdit.h"
#include "XControlManager.h"
#include "XGroupbox.h"

CXEdit::CXEdit()
{
	SetClass(_T("EDIT"));
	SetStyle(WS_XEDIT);
}

CXEdit::~CXEdit()
{
}

void CXEdit::SetText(string szText)
{
	CXControl::SetText(szText);
	Edit_SetText(hWnd, szText.c_str());
}

string CXEdit::GetText()
{
	szText.resize(Edit_GetTextLength(hWnd) + 1, 0);
	Edit_GetText(hWnd, &szText[0], (int)szText.size());
	return szText;
}

void CXEdit::SetDimensions(uint w, uint h, bool bUpdate)
{
	if (!h)
	{
		TEXTMETRIC tm;
		HDC hdc = GetDC(pOwner->GetHandle());
		SelectObject(hdc, pOwner->GetFont()->Get());
		GetTextMetrics(hdc, &tm);
		uHeight = tm.tmHeight + tm.tmHeight / 2;
	}
	if (!w)
		uWidth = DEFAULT_EDIT_WIDTH;
	CXTextSizedControl::SetDimensions(w, h, bUpdate);
}

void CXEdit::SetBuddyLabel(string szText, EBUDDYPOS eBuddyPos, PXControl pGroupbox)
{
	POINT ptLbl = GetBuddyPos(szText, eBuddyPos);
	if (!xBuddyLblID)
	{
		auto cm = pOwner->_dpc<CXControlMgrWindow>()->GetControlManager();
		auto pLbl = cm->CreateLabel(Label_MakeCP(0, ptLbl.x, ptLbl.y, 0, 0, szText));
		xBuddyLblID = pLbl->GetID();
		if (pGroupbox)
			pGroupbox->_dpc<CXGroupbox>()->AddControl(pLbl);
	}
	else
	{
		auto pCtrl = CXControl::Get(xBuddyLblID);
		pCtrl->SetText(szText);
		pCtrl->SetPos(ptLbl.x, ptLbl.y);
	}
}

POINT CXEdit::GetBuddyPos(string szText, EBUDDYPOS eBuddyPos)
{
	POINT ptBuddy{ 0 };
	RECT rcText = GetTextRect(szText);
	POINT pos = GetPos();
	TEXTMETRIC tm = GetTextMetric();
	switch (eBuddyPos)
	{
		case EB_LEFT:
		{
			ptBuddy.x = pos.x - rcText.right - 5;
			ptBuddy.y = pos.y + (tm.tmHeight / 4);
			break;
		}
		case EB_RIGHT:
		{
			ptBuddy.x = pos.x + uWidth + 5;
			ptBuddy.y = pos.y + (tm.tmHeight / 4);
			break;
		}
		case EB_TOPLEFT:
		{
			ptBuddy.x = pos.x;
			ptBuddy.y = pos.y - tm.tmHeight;
			break;
		}
		case EB_TOPRIGHT:
		{
			ptBuddy.x = pos.x + uWidth - rcText.right;
			ptBuddy.y = pos.y - tm.tmHeight;
			break;
		}
		case EB_BOTTOMLEFT:
		{
			ptBuddy.x = pos.x;
			ptBuddy.y = pos.y + tm.tmHeight + (tm.tmHeight / 2);
			break;
		}
		case EB_BOTTOMRIGHT:
		{
			ptBuddy.x = pos.x + uWidth - rcText.right;
			ptBuddy.y = pos.y + tm.tmHeight + (tm.tmHeight / 2);
			break;
		}
	}

	return ptBuddy;
}