#pragma once
#include "XFunctionalControl.h"

class CXCollectionItem
{
	void* pItemData = nullptr;
	string szText;
public:
	CXCollectionItem(string szText, void* pItemData = nullptr)
	{
		this->szText = szText;
		this->pItemData = pItemData;
	}
	string GetText() { return szText; }
	void* GetDataPtr() { return pItemData; }
};

typedef CXCollectionItem CXItem;
typedef std::vector<CXItem> VXCollection;

class CXCollectionControl// :	public CXFunctionalControl
{
public:
	CXCollectionControl();
	~CXCollectionControl();
	CXCollectionControl(CXItem xItem) { AddItem(xItem); }
	CXCollectionControl(string szText) { AddItem(szText); }
	CXCollectionControl(string szText, void* pItemData) { AddItem(szText, pItemData); }

	virtual void AddItem(CXItem xItem);
	virtual void AddItem(string szText);
	virtual void AddItem(string szText, void* pItemData);
	virtual CXItem GetItem(uint uIndex);
	virtual string GetItemText(uint uIndex);
	virtual void* GetItemDataPtr(uint uIndex);
	virtual void RemoveItem(uint uIndex);

private:
	VXCollection vItems;
};
