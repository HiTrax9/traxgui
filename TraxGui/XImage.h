#pragma once
#include "XMouseTrackingControl.h"

#define WS_XIMAGE WS_CHILD | WS_VISIBLE | SS_OWNERDRAW | SS_NOTIFY

class CXImage;
typedef std::shared_ptr<CXImage> PXImage;

class CXImage : public CXMouseTrackingControl
{
public:
	CXImage();
	~CXImage();
	virtual PXImage Set(HANDLE hImage);
	virtual HANDLE Load(string szFilepath);

protected:
	DWORD dwLoadFlags = 0;
	HANDLE hImage = 0;
};

class CXIcon;
typedef std::shared_ptr<CXIcon> PXIcon;

class CXIcon : public CXImage
{
	void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);
};

class CXBitmap;
typedef std::shared_ptr<CXBitmap> PXBitmap;

class CXBitmap : public CXImage
{
	void Draw(HTHEME hTheme, LPDRAWITEMSTRUCT lpDI);
};

#define Icon_MakeCP(id, x, y) CXControl::MakeCP(id, x, y, 0, 0, _T(""), EICON)
#define _Icon_MakeCP(id, x, y, w, h) CXControl::MakeCP(id, x, y, w, h, _T(""), EICON)
#define Bitmap_MakeCP(id, x, y) CXControl::MakeCP(id, x, y, 0, 0, _T(""), EBITMAP)
#define _Bitmap_MakeCP(id, x, y, w, h) CXControl::MakeCP(id, x, y, w, h, _T(""), EBITMAP)