#pragma once
#include "XGuiBase.h"
#include "XMenu.h"

#define DEFAULT_WINDOW_WIDTH  600
#define DEFAULT_WINDOW_HEIGHT 400
#define DEF_X_POS 400
#define DEF_Y_POS 250

class CXWindow;
typedef std::shared_ptr<CXWindow> PXWindow;
typedef std::map<HWND, PXWindow> XWindowMap;

class CXWindow : public std::enable_shared_from_this<CXWindow>
{
public: // static stuff
	static XWindowMap mWindows;
	static PXWindow GetXWindow(HWND hWnd);

public:
	CXWindow();
	CXWindow(HINSTANCE hInstance, WNDPROC WndProc);
	~CXWindow();

public: //shared ptr helper/cast functions
	// this needs to be called AFTER the ctor!
	virtual PXWindow GetPointer();
	// templated cast function
	template <class T>
	std::shared_ptr<T> _dpc();

public: // creation functions
	virtual WNDCLASSEX CreateWindowClass(string& szClass);
	virtual int Create(bool bRegisterClass = false);

public: // getters
	virtual HWND GetHandle();
	virtual HINSTANCE GetInstance();
	virtual WNDPROC GetWndProc();
	virtual RECT GetCRect();
	virtual string GetText();
	virtual XID GetID();
	virtual PXFont GetFont();
	virtual PXColor GetBGColor();
	virtual PXColor GetFontColor();
	virtual HDC GetWndDC();
	virtual DWORD GetStyle();
	virtual DWORD GetStyleEx();
	virtual uint GetWidth();
	virtual uint GetHeight();
	virtual POINT GetPos();
	virtual TEXTMETRIC GetTextMetric();
	virtual bool HasMenu();

public: // setters
	virtual void SetInstance(HINSTANCE hInstance);
	virtual void SetWndProc(WNDPROC WndProc);
	virtual void SetID(XID xID);
	virtual void SetOwner(PXWindow pOwner);
	virtual void SetClass(string szClass);
	virtual void SetStyle(DWORD dwStyle);
	virtual void SetStyleEx(DWORD dwStyleEx);
	virtual void SetText(string szText);
	virtual void SetPos(int x, int y, bool bUpdate = true);
	virtual void SetHeight(uint h, bool bUpdate = true);
	virtual void SetWidth(uint w, bool bUpdate = true);
	virtual void SetDimensions(uint w, uint h, bool bUpdate = true);
	virtual void SetBgColor(PXColor xColor);
	virtual void SetBgColor(COLORREF xColor);
	virtual void SetFont(FontID fontID);
	virtual void SetTextColor(PXColor xColor);
	virtual void SetTextColor(COLORREF xColor);
	virtual void SetCursor(HCURSOR hCursor);
	virtual void SetMenu(PXMenu xMenu);
	virtual void SetContextMenu(PXMenu xMenu);
	virtual void MenuHandler(XID idItem);

public: // notifications for controls/windows to override
	virtual void Clear();
	virtual void Redraw();
	virtual void Destroy();
	virtual HRESULT OnCreate(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnInitDialog(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnNotify(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnSetCursor(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnMouseMove(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnSetFocus(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnKillFocus(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnCommand(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnDrawItem(WPARAM wParam, LPARAM lParam) { return 0; }
	virtual HRESULT OnContextMenu(WPARAM wParam, LPARAM lParam);
	virtual HRESULT OnCtlColor(WPARAM wParam, LPARAM lParam);
protected:
	PXWindow pOwner;
	XID xID = 0;
	HINSTANCE hInstance = 0;
	WNDPROC WndProc = 0;
	HWND hWnd = 0;
	string szClass;
	string szText;
	string szMenu;
	DWORD dwStyle = 0;
	DWORD dwStyleEx = 0;

	HICON hIcon = 0;
	HICON hIconSm = 0;
	HCURSOR hCursor = 0;
	HCURSOR hCursorOld = 0;

	PXColor colBG; // initialized in the ctor.
	PXColor colText;
	uint uWidth = 0;
	uint uHeight = 0;
	int iX = 0;
	int iY = 0;
	bool bShow = true;

	bool bHasFont = false;
	FontID fontID = -1;

	bool bHasMenu = false;
	PXMenu pMenu = nullptr;
	PXMenu pContextMenu = nullptr;
};

template<class T>
inline std::shared_ptr<T> CXWindow::_dpc()
{
	return std::dynamic_pointer_cast<T, CXWindow>(GetPointer());
}
